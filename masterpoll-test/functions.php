<?php

# Sistema delle informazioni sull'invio di una request da Telegram 
# Controlla le variabili già esistenti per utilizzare al meglio il Framework
if (isset($update)) {
	# Sistemazione update del Bot in base alla configurazione
	if (isset($update['channel_post'])) { // Post inviato su un canale
		if ($config['post_canali']) {
			$update['message'] = $update['channel_post'];
			$dacanale = true;
			$update['message']['chat']['typechat'] = 'channel';
		} else {
			exit;
		}
	}
	if (isset($update['edited_channel_post'])) { // Post modificato su un canale
		if ($config['post_canali']) {
			$modificato = true;
			$dacanale = true;
			$update['message'] = $update['edited_channel_post'];
		} else {
			exit;
		}
	}
	if (isset($update['edited_message'])) {// Messaggio modificato
		if ($config['modificato']) {
			$update['message'] = $update['edited_message'];
			$modificato = true;
		} else {
			exit;
		}
	}

	# Imformazioni utente su un canale
	if (isset($update['message']['author_signature']) and $config['post_canali']) {
		$firma = $update['message']['author_signature']; // Firma del Post su un canale
	}
	# Informazioni utente su un messaggio inoltrato dal canale
	if (isset($update['message']['forward_signature']) and $config['post_canali']) {
		$ffirma = $update['message']['forward_signature']; // Firma del Post su un canale
	}
	# Informazioni utente
	if (isset($update['chosen_inline_result']['from'])) $update['message']['from'] = $update['chosen_inline_result']['from'];
	if (isset($update['message']['from'])) {
		$exists_user = true;
		$userID = $update['message']['from']['id'];
		$nome = $update['message']['from']['first_name'];
		$cognome = $update['message']['from']['last_name'];
		$username = $update['message']['from']['username'];
		$lingua = $update['message']['from']['language_code'];
	}
	# Informazioni utente inoltrato
	if (isset($update['message']['forward_from'])) {
		$exists_fuser = true;
		$fuserID = $update['message']['forward_from']['id'];
		$fnome = $update['message']['forward_from']['first_name'];
		$fcognome = $update['message']['forward_from']['last_name'];
		$fusername = $update['message']['forward_from']['username'];
		$flingua = $update['message']['forward_from']['language_code'];
	}

	# Messaggio inviato
	$messageType = "text message";
	$msg = $update['message']['text']; // Testo del messaggio inviato (Vale anche per quelli inoltrati)
	$entities = $update['message']['entities']; // Entità del messaggio inviato (Vale anche per quelli inoltrati)
	$msgID = $update['message']['message_id']; // ID del messaggio inviato
	$caption = $update['message']['caption']; // Testo che si trova nei file media
	# Messaggio sulla risposta
	if (isset($update['message']['reply_to_message'])) {
		$reply = true;
		$rmsg = $update['message']['reply_to_message']['text']; // Testo del messaggio al quale si risponde
		$rentities = $update['message']['reply_to_message']['entities']; // Entità del messaggio al quale si risponde
		$rmsgID = $update['message']['reply_to_message']['message_id']; // ID del messaggio al quale si risponde
		$rdata = $update['message']['reply_to_message']['date']; // Data del messaggio in reply
		
		# Informazioni utente sulla reply
		if (isset($update['message']['reply_to_message']['from'])) {
			$exists_ruser = true;
			$ruserID = $update['message']['reply_to_message']['from']['id'];
			$rnome = $update['message']['reply_to_message']['from']['first_name'];
			$rcognome = $update['message']['reply_to_message']['from']['last_name'];
			$rusername = $update['message']['reply_to_message']['from']['username'];
			$rlingua = $update['message']['reply_to_message']['from']['language_code'];
		} 
	
		# Informazioni utente inoltrato sulla reply
		if (isset($update['message']['reply_to_message']['forward_from'])) {
			$exists_rfuser = true;
			$rfuserID = $update['message']['reply_to_message']['forward_from']['id'];
			$rfnome = $update['message']['reply_to_message']['forward_from']['first_name'];
			$rfcognome = $update['message']['reply_to_message']['forward_from']['last_name'];
			$rfusername = $update['message']['reply_to_message']['forward_from']['username'];
			$rflingua = $update['message']['reply_to_message']['forward_from']['language_code'];
		} 
	}

	# Date e orari [Timestamp]
	$data = $update['message']['date']; // Data dell'invio del Messaggio (Vale anche per quelli inoltrati)
	if (isset($modificato)) {
		$edata = $update['message']['edit_date']['date']; // Data dell'ultima modifica sul messagio
	}
	$fdata = $update['message']['forward_date']; // Data del messaggio inoltrato

	# Gruppi e Canali
	$chatID = $update['message']['chat']['id'];  // ID del gruppo/canale
	$typechat = $update['message']['chat']['type']; // Tipo di chat (private, group, supergroup, channel)
	if ($typechat !== "private") {
		$title = $update['message']['chat']['title']; // Titolo del gruppo/canale
		$usernamechat = $update['message']['chat']['username']; // Username del gruppo/canale
	}
	if (isset($update['message']['forward_from_chat'])) {
		$fchatID = $update['message']['forward_from_chat']['chat']['id']; // ID del gruppo/canale del messaggio inoltrato
		$ftypechat = $update['message']['forward_from_chat']['chat']['type']; // Tipo ci chat (private, group, supergroup, channel) (In base all' inoltro)
		if ($ftypechat !== "private") {
			$ftitle = $update['message']['forward_from_chat']['chat']['title']; // Titolo del canale da cui è stato inoltrato
			$fusernamechat = $update['message']['forward_from_chat']['chat']['username']; // Username del canale da cui è stato inoltrato
		}
	}

	# CallBack Query
	if (isset($update["callback_query"])) {
		$messageType = "callback_query";
		$cbid = $update["callback_query"]["id"]; // ID della query
		$cbdata = $update["callback_query"]["data"]; // Messaggio della query
		
		# Informazioni chat
		$chatID = $update["callback_query"]['message']['chat']['id'];  // ID del gruppo/canale
		$typechat = $update["callback_query"]['message']['chat']['type']; // Tipo di chat (private, group, supergroup, channel)
		if ($typechat !== "private") {
			$title = $update["callback_query"]['message']['chat']['title']; // Titolo del gruppo/canale
			$usernamechat = $update["callback_query"]['message']['chat']['username']; // Username del gruppo/canale
		}
		
		# Informazioni utente
		if (isset($update['callback_query']['from'])) {
			$exists_user = true;
			$userID = $update['callback_query']['from']['id'];
			$nome = $update['callback_query']['from']['first_name'];
			$cognome = $update['callback_query']['from']['last_name'];
			$username = $update['callback_query']['from']['username'];
			$lingua = $update['callback_query']['from']['language_code'];
		} 
		
		# Messaggio dal CallBack
		if (isset($update["callback_query"]["inline_message_id"])) { // CallBack per i messaggi inline
			$cbmid = $update["callback_query"]["inline_message_id"]; // ID del messaggio mandato inline nella query
			$chatID = $userID; // ID della Chat sulla query
		} else {
			$cbmid = $update["callback_query"]["message"]["message_id"]; // ID del messaggio nella query
			$chatID = $update["callback_query"]["message"]["chat"]["id"]; // ID della Chat sulla query
		}
	}

	# Media
	if (isset($update["message"]["voice"])) {
		$messageType = "voice";
		$vocale = $update["message"]["voice"]["file_id"]; // ID del audio vocale inviato
	}
	if (isset($update["message"]["animation"])) {
		$messageType = "animation";
		$gif = $update["message"]["animation"]["file_id"]; // ID della GIF inviata
	}
	if (isset($update["message"]["photo"])) {
		$messageType = "photo";
		$foto = $update["message"]["photo"][count($update["message"]["photo"]) -1]["file_id"]; // ID della foto inviata a minima qualità
	}
	if (isset($update["message"]["document"]) and !isset($gif)) {
		$messageType = "document";
		$file = $update["message"]["document"]; // ID del file inviato
	}
	if (isset($update["message"]["video"])) {
		$video = $update["message"]["video"]['file_id']; // ID del video inviato
		$messageType = "video";
	}
	if (isset($update["message"]["video_note"])) {
		$video_note = $update["message"]["video_note"]['file_id']; // ID del video rotondo inviato
		$messageType = "video_note";
	}
	if (isset($update["message"]["audio"])) {
		$audio = $update["message"]["audio"]["file_id"]; // ID del file audio inviato
		$messageType = "audio";
	}
	if (isset($update["message"]['location'])) {
		$messageType = "location";
		$posizione = $update["message"]['location']; // Posizione del GPS inviata
	}
	if (isset($update["message"]["sticker"])){ // Update per le stickers{
		$messageType = "sticker";
		$s_setname = $update["message"]["sticker"]["set_name"]; // Nome del Pacchetto Sticker
		$sticker = $update["message"]["sticker"]["file_id"]; // ID dello Sticker inviato
		$s_emoji = $update["message"]["sticker"]["emoji"]; // Emoji attribuito allo Sticker inviato
		$s_x = $update["message"]["sticker"]["width"]; // Larghezza dell'immagine Sticker
		$s_y = $update["message"]["sticker"]["height"]; // Altezza dell'immagine Sticker
		$s_bytes = $update["message"]["sticker"]["file_size"]; // Peso dello Sticker espresso in byte
	}
	if (isset($update['message']['contact'])) { // Update per i contatti
		$messageType = "contact";
		$contact = $update['message']['contact']['phone_number']; // Numero del contatto
		$cnome = $update['message']['contact']['first_name']; // Nome del Contatto
		$ccognome = $update['message']['contact']['last_name']; // Cognome del Contatto
		$cuserID = $update['message']['contact']['user_id']; // ID dell'utente del contatto
	}
	if (isset($update['message']['venue'])) { // Update per i venue
		$messageType = "venue";
		$venue = true;
		$posto = $update['message']['venue']['title']; // Titolo della posizione
		$address = $update['message']['venue']['address']; // Indirizzo della posizione
		$posizione = $update['message']['venue']['location']; // Posizione del GPS inviata
	}
	if (isset($update["inline_query"])) {
		$messageType = "inline";
		$chatID = $userID = $update["inline_query"]["from"]["id"];
		$nome = $update["inline_query"]["from"]["first_name"];
		$cognome = $update["inline_query"]["from"]["last_name"];
		$username = $update["inline_query"]["from"]["username"];
		$lingua = $update["inline_query"]["from"]["language_code"];
		$exists_user = true;
		$inline = true;
	} 

	if (in_array($msg[0], $config['operatori_comandi'])) {
		if ($dacanale) {
			
		} else {
			$messageType = "command";
			$cmd = substr($msg, 1, strlen($msg));
			$cmd = str_replace("@" . $config['username_bot'], '', $cmd); //Fix del Tag al Bot nei gruppi
		}
	}
	if ($msg === 0) {
		$msg = "0";
		$messageType = "text message";
	}
	if ($chatID === $config['console'] and !$cbdata and $typechat == "channel") die;

	# Fine gestione variabili
}

#Funzioni del Bot

# Segnalazione errori php
set_error_handler("errorHandler");
register_shutdown_function("shutdownHandler");
function errorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
    global $config;
    $error = $error_message . " \nLine: " . $error_line;
    switch ($error_level) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_PARSE:
            if ($config['log_report']['FATAL']) {
                call_error("FATAL: " . $error, $error_file);
            }
            break;
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
            if ($config['log_report']['ERROR']) {
                call_error("ERROR: " . $error, $error_file);
            }
            break;
        case E_WARNING:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_USER_WARNING:
            if ($config['log_report']['WARN']) {
                call_error("WARNING: " . $error, $error_file);
            }
            break;
        case E_NOTICE:
        case E_USER_NOTICE:
            if ($config['log_report']['INFO']) {
                call_error("INFO: " . $error, $error_file);
            }
            break;
        case E_STRICT:
            if ($config['log_report']['DEBUG']) {
                call_error("DEBUG: " . $error, $error_file);
            }
            break;
        default:
            if ($config['log_report']['WARN']) {
                call_error("WARNING: " . $error, $error_file);
            }
    }
}
function shutdownHandler() {
    global $config;
    global $PDO;
    global $redis;
    if (isset($PDO)) unset($PDO);
    if (isset($redis)) unset($redis);
    $lasterror = error_get_last();
    switch ($lasterror['type']) {
        case E_ERROR:
        case E_CORE_ERROR:
        case E_COMPILE_ERROR:
        case E_USER_ERROR:
        case E_RECOVERABLE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_WARNING:
        case E_PARSE:
			fastcgi_finish_request();
            if ($config['log_report']['SHUTDOWN']) {
                $error = $lasterror['message'] . " \nLine: " . $lasterror['line'];
                call_error($error, $lasterror['file']);
            }
    }
}

// Metodo di request (cURL e Json Payload)
function sendRequest($url = false, $args = false, $response = 'def', $metodo = 'def') {
	global $config;
	global $ch_api;
	global $api;
	global $redis;
	global $botID;
	global $userID;
	global $time_run;
	if (!$url) {
		return false;
	}
	if ($response === 'def') {
		$response = $config['response'];
	}
	if ($metodo === 'def') {
		if (strtolower($config['method']) == 'post') {
			$post = true;
		} else {
			$post = false;
		}
	} elseif (strtolower($metodo) == 'post') {
		$post = true;
	} else {
		$post = false;
	}
	if (isset($args['text'])) $args['text'] = mb_convert_encoding($args['text'], "UTF-8");
	if (!defined('json_payload') and $config['json_payload'] and !$response and strpos($url, 'api.telegram.org/') !== false) {
		define('json_payload', false);
		$method = explode('/', $url);
		$method = $method[count($method)-1];
		$args['method'] = $method;
		$json = json_encode($args);
		header('Content-Type: application/json');
		header('Content-Length: ' . strlen($json));
		$error = error_get_last();
		if (strpos($error['message'], "Cannot modify header information") !== false) {
			unset($args['method']);
			sendRequest($url, $args, 0);
			return json_encode(['result' => "Json Payload failed", 'error' => $error, "contents" => $ob_contents]);
		} else {
			echo $json;
			fastcgi_finish_request();
			ob_end_clean();
		}
		ob_start();
		return json_encode(['result' => "Json Payload"]);
	} else {
		define('json_payload', false);
		if (!isset($ch_api)) {
			$ch_api = curl_init();
		}
		if (!$post) {
			if ($args) { $url .= "?" . http_build_query($args); }
			curl_setopt_array($ch_api, [
				CURLOPT_URL => $url,
				CURLOPT_POST => 0,
				CURLOPT_CONNECTTIMEOUT => $config['request_timeout'],
				CURLOPT_RETURNTRANSFER => $response
			]);
		} else {
			curl_setopt_array($ch_api, [
				CURLOPT_URL => $url,
				CURLOPT_POST=> 1,
				CURLOPT_POSTFIELDS => $args,
				CURLOPT_CONNECTTIMEOUT => $config['request_timeout'],
				CURLOPT_RETURNTRANSFER => $response
			]);
		}
		ob_end_clean();
		ob_start();
		$output = curl_exec($ch_api);
		/*
		$start_request = microtime(true);
		$end_request = microtime(true);
		if (isset($redis)) {
			if (round($end_request - $start_request) >= 1) {
				rset('secure_mode' . $botID, time() + 5);
			}
		}
		*/
		if (!$response) {
			$obcontents = ob_get_contents();
			ob_end_clean();
			ob_start();
			fastcgi_finish_request();
			if (json_decode($obcontents, true)) {
				return $obcontents;
			}
			return json_encode(['result' => "No response"]);
		}
		fastcgi_finish_request();
		return $output;
	}
}

// Avvisi Errori
function call_error($error = "Null", $plugin = 'no', $chat = 'def') {
	global $api;
	global $config;
	global $pluginp;
	global $poll_id;
	global $creator;
	global $rupdate;
	if (!$config['console']) return false;
	$time = time();
	$userbot = $config['username_bot'];
	if ($chat == 'def') {
		$chat = $config['console'];
	}
	if (!$pluginp) {
	} elseif ($plugin == 'no') {
		$plugin = $pluginp;
	}
	if (isset($poll_id) or isset($creator)) $sondaggio = "\n<b>Sondaggio:</b> $poll_id-$creator";
	$text = "#Error ‼️\n$error \n" . bold("Plugin:") . " $plugin $sondaggio\n<b>Codice:</b> $time \n@$userbot";
	$args = [
		'chat_id' => $chat,
		'text' => $text,
		'parse_mode' => 'html',
	];
	sendRequest("https://api.telegram.org/bot{$config['logs_token']}/sendMessage", $args, false, 'post');
	/*if ($config['devmode']) {
		echo "<br><b>Bot Error:</b> $error in $plugin<br>";
	}*/
	ob_end_clean();
	ob_start();
}

// Database
if ($config['usa_il_db'] and $config['usa_redis']) {

	// Query semplice per PDO
	function db_query($query, $args = false, $fetch = false) {
		global $PDO;
		if (!$PDO) {
			call_error("#PDOError \nQuery: " . code($query) . " \nDatabase non avviato.");
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		try {
			$q = $PDO->prepare($query);
			$db_query['ok'] = true;
		} catch (PDOException $e) {
			$db_query['ok'] = false;
			$db_query['error'] = $e;
			return $db_query;
		}
		if (is_array($args)) {
			$q->execute($args);
			$db_query['args'] = $args;
		} else {
			$q->execute();
		}
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			$db_query['ok'] = false;
			call_error("PDO Error\n<b>INPUT:</> " . code($query) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query['error_code'] = $error[0];
			$db_query['description'] = "[MYSQL]" . $error[2];
			return $db_query;
		}
		if ($fetch) {
			$db_query['result'] = $q->fetch(\PDO::FETCH_ASSOC);
		} elseif ($fetch == "no") {
			$db_query['result'] = true;
		} else {
			$db_query['result'] = $q->fetchAll();
		}
		return $db_query;
	}
	function getName($userID = false) {
		global $PDO;
		global $config;
		if (!$userID) {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$userID was not found", 'result' => "Unknown user"];
		} elseif (!is_numeric($userID)) {
			return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be a numeric value", 'result' => "Unknown user"];
		}
		if (!$PDO) {
			return ['ok' => false, 'error_code' => 500, 'description' => "Internal Server Error: database not started"];
		}
		if ($config['usa_redis']) {
			$nomer = rget("name$userID");
			if ($nomer['ok'] and $nomer['result'] and !isset($nomer['result']['ok'])) return ['ok' => true, 'redis' => $nomer, 'result' => $nomer['result']];
		}
		$q = $PDO->prepare("SELECT * FROM utenti WHERE user_id = ?");
		$q->execute([$userID]);
		$error = $q->errorInfo();
		if ($error[0] !== "00000") {
			call_error("PDO Error\n<b>INPUT:</> " . code($q) . "\n<b>OUTPUT:</> " . code(json_encode($error)));
			$db_query = [
				'ok' => false,
				'error_code' => $error[0],
				'description' => "[PGSQL]" . $error[2],
				'result' => "Deleted account"
			];
		} else {
			$u = $q->fetchAll()[0];
			if ($u['cognome']) $u['nome'] .= " " . $u['cognome'];
			if (isset($u['nome'])) {
				$rr = htmlspecialchars($u['nome']);
			} else {
				$rr = "Deleted account";
			}
			if ($config['usa_redis']) {
				$res = rset("name$userID", $rr);
			}
			$db_query = [
				'ok' => true,
				'redis' => $res,
				'result' => $rr
			];
		}
		return $db_query;
	}

	// Query per Redis
	function rset($key = null, $data = null) {
		global $redis;
		if (!isset($key)) {
			$redis_query = ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$key was not found"];
		} elseif (!isset($data)) {
			$redis_query = ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$data was not found"];
		} else {
			try {
				$redis_query['ok'] = true;
				$redis_query['ok'] = $redis->set($key, $data);
			} catch (Exception $e) {
				call_error($e->getMessage());
				$redis_query['ok'] = false;
				$redis_query['error_code'] = 500;
				$redis_query['description'] = $e->getMessage();
			}
		}
		return $redis_query;
	}
	function rget($key = null) {
		global $redis;
		if (!isset($key)) {
			$redis_query = ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$key was not found"];
		} else {
			try {
				$redis_query['ok'] = true;
				$redis_query['result'] = $redis->get($key);
			} catch (Exception $e) {
				call_error($e->getMessage());
				$redis_query['ok'] = false;
				$redis_query['error_code'] = 500;
				$redis_query['description'] = $e->getMessage();
			}
		}
		return $redis_query;
	}
	function rdel($key = null) {
		global $redis;
		if (!isset($key)) {
			$redis_query = ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$key was not found"];
		} else {
			try {
				$redis_query['ok'] = true;
				$redis_query['result'] = $redis->del($key);
			} catch (Exception $e) {
				call_error($e->getMessage());
				$redis_query['ok'] = false;
				$redis_query['error_code'] = 500;
				$redis_query['description'] = $e->getMessage();
			}
		}
		return $redis_query;
	}
}

// Query con risposta in Json
function JsonResponse($link, $method = 'def', $args = false)  {
	$r = sendRequest($link, $args, true, $method);
	$rr = json_decode($r, true);
	return $rr;
}

# Integrazione di Bottino
function getBottinoBan (int $user_id) {
	//global $redis;

	if ($redis) {
		$get = $redis->get('bottino-' . $user_id);
		if (!$get['error']) {
			return $get;
		}
	}
	$r = sendRequest("https://bots.bottino.dev/bottino/Source/int/masterpoll.php", ['km' => "ssporjwoeirjhiuewrhbguygruywry23t6712351762guyguygfuyfsdgsuffgy", "user_id" => $user_id], true, "GET");
	$json = json_decode($r, true);
	if ($redis) $redis->set('bottino-' . $user_id, $json);
	return $json;
}

# Funzioni Telegram | Method

//Azioni | sendChatAction
function scAction($chatID, $action = 'typing') {
	global $api;
	$args = array(
		'chat_id' => $chatID,
		'action' => $action
	);
	$rr = sendRequest("https://api.telegram.org/$api/sendChatAction", $args, false);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendChatAction \n<b>INPUT</b>: " . code(json_encode($args)) . " \n<b>OUTPUT:</b> " . $ar['description']);
	}
	return $ar;
}

//Invio messaggi | sendMessage
function sm($chatID, $text, $rmf = false, $pm = 'def', $reply = false, $dislink = 'def', $inline = true) {
	global $api;
	global $config;

	$botapi = $api;
	if ($config['console'] === $chatID) $botapi = "bot" . $config['logs_token'];

	if ($config['usa_redis'] and isset($redis)) {
		$get = rget("sendm_" . "$chatID");
		if ($get['result']) {
			if ($get['result'] <= time()) {
				rset("sendm_" . "$chatID", time() + 1);
			} else {
				return ['ok' => false, 'error_code' => 429, 'description' => "Flood waiting..."];
			}
		} else {
			rset("sendm_" . "$chatID", time() + 1);
		}
	}

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($dislink === 'def') {
		$dislink = $config['disabilita_anteprima_link'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'typing');
	}

	$args = [
		'chat_id' => $chatID,
		'text' => mb_convert_encoding($text, "UTF-8"),
		'parse_mode' => $pm,
		'disable_web_page_preview' => $dislink,
	];
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$botapi/sendMessage", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"]) and $chatID !== $config['console']) {
		if (strpos($ar['description'], "Too Many Requests:") === 0) {
			$time = time() + 10 + str_replace("Too Many Requests: retry after ", '', $ar['description']);
			if ($config['usa_redis'] and isset($redis)) rset("sm_" . "$chatID", $time);
		} elseif ($ar['description'] == "Bad Request: CHAT_ADMIN_REQUIRED") {
			
		} elseif ($ar['description'] == "Bad Request: CHAT_WRITE_FORBIDDEN") {
			
		} elseif ($ar['description'] == "Bad Request: CHANNEL_PRIVATE") {
			setStatus($chatID, 'visto');
		} elseif ($ar['description'] == "Bad Request: need administrator rights in the channel chat") {
			setStatus($chatID, 'attivo');
		} elseif ($ar['description'] == "Forbidden: bot was blocked by the user") {
			setStatus($chatID, 'blocked');
		} elseif ($ar['description'] == "Forbidden: user is deactivated") {
			setStatus($chatID, 'deleted');
		} elseif ($ar['description'] == "Forbidden: bot can't initiate conversation with a user") {
			setStatus($chatID, 'attivo');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} elseif ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif ($ar['description'] == "Forbidden: bot can't send messages to bots") {
			setStatus($chatID, 'bot');
		} elseif ($ar['description'] == "Bad Request: have no rights to send a message") {
			lc($chatID);
		} else {
			call_error("sendMessage \n<b>INPUT</b>: " . code(json_encode($args)) . " \n<b>OUTPUT:</b> " . $ar['description']);
		}
	}
  return $ar;
}

//Rispondi CallBack | editMessageText & answerCallbackQuery
function cb_reply($id, $text = false, $alert = false, $cbmid = false, $ntext = false, $nmenu = false, $pm = 'def', $dislink = 'def') {
	global $api;
	global $chatID;
	global $config;

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($dislink === 'def') {
		$dislink = $config['disabilita_anteprima_link'];
	}

	if (!$text or $text == "null" or $text == null or $text == false) {
		$text = " ";
		if ($cbmid) {
			$c = editMsg($chatID, $ntext, $cbmid, $nmenu, $pm, $dislink);
		} else {
			$c = true;
		}
		$args = [
			'callback_query_id' => $id,
			'text' => $text,
			'show_alert' => $alert,
		];
		sendRequest("https://api.telegram.org/$api/answerCallbackQuery", $args, false);
	} else {
		$args = [
			'callback_query_id' => $id,
			'text' => $text,
			'show_alert' => $alert,
		];
		sendRequest("https://api.telegram.org/$api/answerCallbackQuery", $args, false);
		if ($cbmid) {
			$c = editMsg($chatID, $ntext, $cbmid, $nmenu, $pm, $dislink);
		} else {
			$c = true;
		}
		return $c;
	}
}

function cb_url($cbid, $url) {
	global $api;

	$args = [
		'callback_query_id' => $cbid,
		'url' => $url
	];

	$rr = sendRequest("https://api.telegram.org/$api/answerCallbackQuery", $args, false);
	return $rr;
}

function media_cb_reply($id, $text = '', $alert = false, $cbmid = false, $file_id = false, $type = 'def', $caption = '', $nmenu = false, $pm = 'def') {
	global $api;
	global $chatID;
	global $config;
	
	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	$args = [
		'callback_query_id' => $id,
		'text' => $text,
		'show_alert' => $alert,
	];
	sendRequest("https://api.telegram.org/$api/answerCallbackQuery", $args, false);
	if ($cbmid) {
		$c = editMedia($chatID, $cbmid, $file_id, $type, $caption, $nmenu, $pm);
	} else {
		$c = true;
	}
	return $c;
}

function editMenu($chatID, $cbmid, $editKeyBoard = []) {
	global $api;
	$args = [];
	if (is_numeric($cbmid)) {
		$args['chat_id'] = $chatID;
		$args['message_id'] = $cbmid;
	} else {
		$args['inline_message_id'] = $cbmid;
	}
	$args['reply_markup'] = json_encode(['inline_keyboard' => $editKeyBoard]);
	$rr = sendRequest("https://api.telegram.org/$api/editMessageReplyMarkup", $args);
	$ar = json_decode($rr, true);
	return $ar;
}

//Modifica il testo di un messaggio | editMessageText
function editMsg($chatID, $msg, $cbmid, $editKeyBoard = false, $pm = 'html', $dislink = 'def') {
	global $api;
	global $config;
	global $redis;
	
	/*if ($config['usa_redis'] and isset($redis)) {
		$get = rget("editmsg_" . "$chatID-$cbmid");
		if ($get['result']) {
			if ($get['result'] <= time()) {
				rset("editmsg_" . "$chatID-$cbmid", time() + 1);
			} else {
				return ['ok' => false, 'error_code' => 429, 'description' => "Flood waiting..."];
			}
		} else {
			rset("editmsg_" . "$chatID-$cbmid", time() + 1);
		}
	}*/
	
	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($dislink === 'def') {
		$dislink = $config['disabilita_anteprima_link'];
	}

	$args = [
		'text' => $msg,
		'parse_mode' => $pm,
		'disable_web_page_preview' => $dislink
	];
	if (is_numeric($cbmid)) {
		$args['chat_id'] = $chatID;
		$args['message_id'] = $cbmid;
	} else {
		$args['inline_message_id'] = $cbmid;
	}

	if ($editKeyBoard) {
		$args["reply_markup"] = json_encode(['inline_keyboard' => $editKeyBoard]);
	}

	$rr = sendRequest("https://api.telegram.org/$api/editMessageText", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if (strpos($ar['description'], "Too Many Requests:") === 0) {
			/*$time = time() + 10 + str_replace("Too Many Requests: retry after ", '', $ar['description']);
			if ($config['usa_redis'] and isset($redis)) rset("editmsg_" . "$chatID-$cbmid", $time);*/
		} elseif ($ar['description'] == "Bad Request: MESSAGE_ID_INVALID") {
			
		} elseif ($ar['description'] == "Bad Request: CHAT_ADMIN_REQUIRED") {
			
		} elseif ($ar['description'] == "Bad Request: message is not modified: specified new message content and reply markup are exactly the same as a current content and reply markup of the message") {
			
		} elseif ($ar['description'] == "Bad Request: message to edit not found") {
			
		} elseif ($ar['description'] == "Bad Request: message can't be edited") {
			
		} elseif ($ar['description'] == "Bad Request: need administrator rights in the channel chat") {
			setStatus($chatID, 'attivo');
		} elseif ($ar['description'] == "Bad Request: can't access the chat") {
			setStatus($chatID, 'deleted');
		} elseif ($ar['description'] == "Forbidden: user is deactivated") {
			setStatus($chatID, 'deleted');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} elseif ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} else {
			call_error("editMessageText \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Modifica il testo di un file media | editMessageCaption
function editMsgc($chatID, $msg, $cbmid, $editKeyBoard = false, $pm = 'html', $dislink = 'def') {
	global $api;
	global $config;

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($dislink === 'def') {
		$dislink = $config['disabilita_anteprima_link'];
	}

	$args = array(
		'chat_id' => $chatID,
		'text' => $msg,
		'message_id' => $cbmid,
		'parse_mode' => $pm,
		'disable_web_page_preview' => $dislink
	);

	if ($editKeyBoard) {
		$rm = array('inline_keyboard' => $editKeyBoard);
		$rm = json_encode($rm);
		$args["reply_markup"] = $rm;
	}

	$rr = sendRequest("https://api.telegram.org/$api/editMessageCaption", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("editMessageCaption \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Modifica un file media | editMessageMedia
function editMedia($chatID, $msgID, $file_id, $type = 'def', $caption = false, $editKeyBoard = false, $pm = 'def') {
	global $api;
	global $config;
	global $messageType;
	
	if ($type === 'def') {
		$type = $messageType;
	}

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}
	
	$media = array(
		'type' => $type,
		'media' => $file_id,
		'parse_mode' => $pm
	);

	if ($caption) {
		$media['caption'] = $caption;
	}

	$args = array(
		'chat_id' => $chatID,
		'message_id' => $msgID,
		'media' => json_encode($media)
	);

	if ($editKeyBoard) {
		$rm = array('inline_keyboard' => $editKeyBoard);
		$rm = json_encode($rm);
		$args['reply_markup'] = $rm;
	}

	$rr = sendRequest("https://api.telegram.org/$api/editMessageMedia", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} else {
			call_error("editMessageMedia\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Elimina un messaggio | deleteMessage
function dm($chatID, $msgID) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'message_id' => $msgID
	];

	$rr = sendRequest("https://api.telegram.org/$api/deleteMessage", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		//call_error("deleteMessage\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Inoltra un messaggio | forwardMessage
function fw($chatID, $fromID, $msgID) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'from_chat_id' => $fromID,
		'message_id' => $msgID
	];

	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	$rr = sendRequest("https://api.telegram.org/$api/forwardMessage", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Forbidden: bot was blocked by the user") {
			setStatus($chatID, 'blocked');
		} elseif ($ar['description'] == "Forbidden: user is deactivated") {
			setStatus($chatID, 'deleted');
		} elseif ($ar['description'] == "Forbidden: bot can't initiate conversation with a user") {
			setStatus($chatID, 'attivo');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} elseif ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif ($ar['description'] == "Forbidden: bot can't send messages to bots") {
			setStatus($chatID, 'bot');
		} else {
			call_error("forwardMessage\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Invia una foto | sendPhoto
function sp($chatID, $photo, $caption = null, $rmf = false, $pm = 'def', $reply = false, $inline = true) {
	global $api;
	global $config;

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_photo');
	}

	$args = array(
		'chat_id' => $chatID,
		'photo' => $photo,
		'caption' => $caption,
		'parse_mode' => $pm
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendPhoto", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendPhoto\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un file audio | sendAudio
function sa($chatID, $audio, $caption = null, $rmf = false, $pm = 'def', $reply = false, $inline = true) {
	global $api;
	global $config;

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_audio');
	}

	$args = array(
		'chat_id' => $chatID,
		'audio' => $audio,
		'caption' => $caption,
		'parse_mode' => $pm
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendAudio", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendAudio\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un audio vocale | sendVoice
function sav($chatID, $audio, $caption = null, $rmf = false, $pm = 'def', $reply = false, $inline = true) {
	global $api;
	global $config;


	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'record_audio');
	}

	$args = array(
		'chat_id' => $chatID,
		'voice' => $audio,
		'caption' => $caption,
		'parse_mode' => $pm
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendVoice", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendVoice\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un video | sendVideo
function sv($chatID, $documento, $caption = null, $rmf = false, $pm = 'def', $reply = false, $inline = true) {
	global $api;
	global $config;


	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_video');
	}

	$args = array(
		'chat_id' => $chatID,
		'video' => $documento,
		'caption' => $caption,
		'parse_mode' => $pm
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendVideo", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendVideo\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un video rotondo | sendVideoNote
function svr($chatID, $video_note, $rmf = false, $reply = false, $inline = true) {
	global $api;
	global $config;

	if ($config['azioni']) {
		scAction($chatID, 'upload_video_note');
	}

	$args = array(
		'chat_id' => $chatID,
		'video_note' => $video_note,
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendVideoNote", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendVideoNote\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un contatto | sendContact
function sc($ID, $numero, $firstname = "Sconosciuto", $lastname = " ") {
	global $api;

	$args = array(
		'chat_id' => $ID,
		'phone_number' => $numero,
		'first_name' => $firstname,
		'last_name' => $lastname
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	$rr = sendRequest("https://api.telegram.org/$api/sendContact", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendContact\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia un file | sendDocument
function sd($chatID, $documento, $caption = null, $rmf = false, $pm = 'def', $reply = false, $thumb = 'def', $inline = true) {
	global $api;
	global $f;
	global $config;
	global $redis;

	$botapi = $api;
	if ($config['console'] === $chatID) $botapi = "bot" . $config['logs_token'];
	
	if ($config['usa_redis'] and isset($redis)) {
		$get = rget("sd_$chatID" . "_$cbmid");
		if ($get['result']) {
			if ($get['result'] <= time()) {
				rset("sd_$chatID" . "_$cbmid", time() + 1);
			} else {
				return ['ok' => false, 'error_code' => 429, 'description' => "Flood waiting..."];
			}
		} else {
			rset("sd_$chatID" . "_$cbmid", time() + 1);
		}
	}

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_document');
	}
	
	if (strpos("http", $documento) == false) {
		$e = explode(".", $documento);
		$ex = $e[count($e) - 1];
		$documento = curl_file_create($documento, "application/" . $ex);
	}
	
	if ($thumb === 'def') {
		$thumb = $f['thumb_logo'];
	}
	
	$args = [
		'chat_id' => $chatID,
		'document' => $documento,
		'thumb' => curl_file_create($thumb, "application/jpeg"),
		'caption' => $caption,
		'parse_mode' => $pm
	];
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendDocument", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if (strpos($ar['description'], "Too Many Requests:") === 0) {
			$time = time() + 10 + str_replace("Too Many Requests: retry after ", '', $ar['description']);
			if ($config['usa_redis'] and isset($redis)) rset("sd_$chatID" . "_$cbmid", $time);
		} else {
			call_error("sendDocument\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Invia una GIF | sendAnimation
function sgif($chatID, $file, $caption = null, $rmf = false, $pm = 'def', $reply = false, $inline = true) {
	global $api;
	global $config;


	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_video');
	}

	$args = array(
		'chat_id' => $chatID,
		'animation' => $file,
		'caption' => $caption,
		'parse_mode' => $pm
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendAnimation", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendAnimation\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia uno sticker | sendSticker
function ss($chatID, $sticker, $rmf = false, $reply = false, $inline = true) {
	global $api;

	$args = array(
		'chat_id' => $chatID,
		'sticker' => $sticker
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendSticker", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendSticker\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia uno dado | sendDice
function sdice($chatID, $rmf = false, $reply = false, $inline = true) {
	global $api;

	$args = [
		'chat_id' => $chatID
	];
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = ['force_reply' => true, 'selective' => true];
	} elseif ($rmf == 'nascondi') {
		$rm = ['hide_keyboard' => true];
	} elseif (!$inline) {
		$rm = ['keyboard' => $rmf, 'resize_keyboard' => true];
	} else {
		$rm = ['inline_keyboard' => $rmf];
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendDice", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendDice\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia delle immagini/video in gruppo | sendMediaGroup
function smg($chatID, $documenti = array(), $caption = 'def', $rmf = false, $pm = 'def', $reply = null, $inline = true) {
	global $api;
	global $config;

	if ($pm === 'def') {
		$pm = $config['parse_mode'];
	}

	if ($config['azioni']) {
		scAction($chatID, 'upload_document');
	}

	if ($caption === 'def') {
	} else {
		$range = range(0, count($documenti) - 1);
		foreach ($range as $num) {
			unset($documenti[$num]['caption']);
			unset($documenti[$num]['parse_mode']);
		}
		$documenti[0]['caption'] = $caption;
		$documenti[0]['parse_mode'] = $pm;
	}

	$args = array(
		'chat_id' => $chatID,
		'media' => json_encode($documenti)
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendMediaGroup", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendMediaGroup\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia una posizione | sendLocation
function sendLocation($chatID, $lati, $long, $rmf = false, $time = null, $reply = false, $inline = true) {
	global $api;
	global $config;

	if ($config['azioni']) {
		scAction($chatID, 'find_location');
	}

	$args = array(
		'chat_id' => $chatID,
		'latitude' => $lati,
		'longitude' => $long,
	);
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	if ($time) {
		$args['live_period'] = $time;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendLocation", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendLocation \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Modifica una posizione in live | editMessageLiveLocation
function editLocation($chatID, $lati, $long, $msgID, $rmf = false) {
	global $api;

	$args = array(
		'chat_id' => $chatID,
		'message_id' => $msgID,
		'latitude' => $lati,
		'longitude' => $long,
	);

	if ($rmf) {
		$rm = array('inline_keyboard' => $rmf);
		$rm = json_encode($rm);
		$args['reply_markup'] = $rm;
	}

	$rr = sendRequest("https://api.telegram.org/$api/editMessageLiveLocation", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("editMessageLiveLocation \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Termina una posizione in live | stopMessageLiveLocation
function stopLocation($chatID, $msgID, $rmf = false) {
	global $api;

	$args = array(
		'chat_id' => $chatID,
		'message_id' => $msgID,
	);

	if ($rmf) {
		$rm = array('inline_keyboard' => $rmf);
		$rm = json_encode($rm);
		$args['reply_markup'] = $rm;
	}

	$rr = sendRequest("https://api.telegram.org/$api/stopMessageLiveLocation", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("stopMessageLiveLocation \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Invia la posizione di un posto | sendVenue
function sven($chatID, $lati, $long, $title, $ind, $rmf = false, $reply = false, $inline = true) {
	global $api;
	global $config;

	if ($config['azioni']) {
		scAction($chatID, 'find_location');
	}

	$args = [
		'chat_id' => $chatID,
		'latitude' => $lati,
		'longitude' => $long,
		'title' => $title,
		'addres' => $ind,
	];
	
	if ($config['disabilita_notifica']) {
		$args['disable_notification'] = true;
	}

	if ($rmf == 'rispondimi') {
		$rm = array('force_reply' => true, 'selective' => true);
	} elseif ($rmf == 'nascondi') {
		$rm = array('hide_keyboard' => true);
	} elseif (!$inline) {
		$rm = array('keyboard' => $rmf, 'resize_keyboard' => true);
	} else {
		$rm = array('inline_keyboard' => $rmf);
	}

	$rm = json_encode($rm);
	if ($rmf) {
		$args['reply_markup'] = $rm;
	}
	if ($reply) {
		$args['reply_to_message_id'] = $reply;
	}
	$rr = sendRequest("https://api.telegram.org/$api/sendVenue", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("sendVenue \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Informazioni di un gruppo/canale | getChat
function getChat($chatID) {
	global $api;

	$args = ['chat_id' => $chatID];

	$rr = sendRequest("https://api.telegram.org/$api/getChat", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: user not found") {
		} elseif ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} else {
			call_error("getChat \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Lista Admins di un gruppo | getChatAdministrators
function getAdmins($chatID) {
	global $api;

	$args = ['chat_id' => $chatID];

	$rr = sendRequest("https://api.telegram.org/$api/getChatAdministrators", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} elseif (in_array($ar['description'], ["Bad Request: channel members are unavailable", "Bad Request: group members are unavailable", "Bad Request: supergroup members are unavailable"])) {
			setStatus($chatID, 'inattivo');
		} else {
			call_error("getChatAdministrators \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Banna un utente | kickChatMember
function ban($chatID, $userID) {
	global $api;
	
	$args = [
		'chat_id' => $chatID,
		'user_id' => $userID
	];
	
	$rr = sendRequest("https://api.telegram.org/$api/kickChatMember", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("kickChatMember\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Sbanna un utente | unbanChatMember
function unban($chatID, $userID) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'user_id' => $userID
	];

	$rr = sendRequest("https://api.telegram.org/$api/unbanChatMember", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("unbanChatMember\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Limita utente (per i gruppi) | restrictChatMember
function limita($chatID, $userID, $durata = null) {
	global $api;

	if ($durata === 'def') {
		$duratas = time();
	} else {
		$duratas = time() + $durata;
	}

	$args = [
		'chat_id' => $chatID,
		'user_id' => $userID,
		'until_date' => $duratas,
		'can_send_messages' => false,
		'can_send_media_messages' => false,
		'can_send_other_messages' => false,
		'can_add_web_page_previews' => false,
	];

	$rr = sendRequest("https://api.telegram.org/$api/restrictChatMember", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("restrictChatMember\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Rendi admin un utente (per i gruppi) | promoteChatMember
function promote($chatID, $userID) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'user_id' => $userID,
		'can_change_info' => false,
		'can_delete_messages' => false,
		'can_invite_users' => false,
		'can_restrict_members' => false,
		'can_pin_messages' => false,
		'can_promote_members' => false,
	];
	$rr = sendRequest("https://api.telegram.org/$api/promoteChatMember", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("promoteChatMember\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Abbandona la chat | leaveChat
function lc($chatID) {
	global $api;

	$args = ['chat_id' => $chatID];

	$rr = sendRequest("https://api.telegram.org/$api/leaveChat", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: PEER_ID_INVALID") {
			
		} elseif ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} elseif (in_array($ar['description'], ["Forbidden: bot was kicked from the channel chat", "Forbidden: bot was kicked from the supergroup chat", "Forbidden: bot was kicked from the group chat", "Bad Request: CHANNEL_PRIVATE"])) {
			setStatus($chatID, 'kicked');
		} else {
			call_error("leaveChat\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Cambia il nome di una Chat (gruppo/canale) | setChatTitle
function setTitle($chatID, $title) {
	global $api;

	$args = [
		'title' => $title,
		'chat_id' => $chatID
	];

	$rr = sendRequest("https://api.telegram.org/$api/setChatTitle", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("setChatTitle\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Cambia la descrizione di una Chat (gruppo/canale) | setChatDescription
function setDescription($chatID, $desc) {
	global $api;

	$args = [
		'description' => $desc,
		'chat_id' => $chatID
	];

	$rr = sendRequest("https://api.telegram.org/$api/setChatDescription", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("setChatDescription\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Setta la foto di una chat (gruppo/canale) | setChatPhoto
function setp($chatID, $photo) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'photo' => $photo
	];

	$rr = sendRequest("https://api.telegram.org/$api/setChatPhoto", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("setChatPhoto \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Elimina la foto di una chat (gruppo/canale) | deleteChatPhoto
function unsetp($chatID) {
	global $api;

	$args = ['chat_id' => $chatID];

	$rr = sendRequest("https://api.telegram.org/$api/deleteChatPhoto", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("deleteChatPhoto \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Setta il set Sticker di un gruppo | setChatStickerSet
function setStickers($chatID, $set) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'sticker_set_name' => $set
	];

	$rr = sendRequest("https://api.telegram.org/$api/setChatStickerSet", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("setChatStickerSet \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Rimuovi il set Sticker di un gruppo | deleteChatStickerSet
function unsetStickers($chatID) {
	global $api;

	$args = ['chat_id' => $chatID];

	$rr = sendRequest("https://api.telegram.org/$api/deleteChatStickerSet", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("deleteChatStickerSet \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Fissa un messaggio (gruppo/canale) | pinChatMessage
function pin($chatID, $rmsgID, $notify = 'def') {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'message_id' => $rmsgID,
		'disable_notification' => false
	];
	
	if (notify === 'def')
	{
		$args['disable_notification'] = $config['disabilita_notifica'];
	} else {
		$args['disable_notification'] = $notify;
	}
	$rr = sendRequest("https://api.telegram.org/$api/pinChatMessage", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("pinChatMessage\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Togli il messaggio fissato (gruppo/canale) | unpinChatMessage
function unpin($chatID, $notify) {
	global $api;

	$args = ['chat_id' => $chatID];

	if (notify === 'def')
	{
		$args['disable_notification'] = $config['disabilita_notifica'];
	} else {
		$args['disable_notification'] = $notify;
	}
	$rr = sendRequest("https://api.telegram.org/$api/unpinChatMessage", $args);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("unpinChatMessage\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Scarica un file da Telegram tramite fileID | getFile
function getFile($fileID) {
	global $api;

	$args = ['file_id' => $fileID];

	$rr = sendRequest("https://api.telegram.org/$api/getFile", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("getFile\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		return $ar["description"];
	}
	return "https://api.telegram.org/file/$api/" . $ar['result']['file_path'];
}

//Ottieni il numero di membri di un Gruppo/Canale | getChatMembersCount
function conta($chatID) {
	global $api;

	$args = array('chat_id' => $chatID);

	$rr = sendRequest("https://api.telegram.org/$api/getChatMembersCount", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} else {
			call_error("getChatMembersCount \n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar["result"];
}

//Esporta il link di un Gruppo | exportChatInviteLink
function getLink($chatID) {
	global $api;

	$args = array('chat_id' => $chatID);

	$rr = sendRequest("https://api.telegram.org/$api/exportChatInviteLink", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} else {
			call_error("exportChatInviteLink\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
		return $ar;
	} else {
		return $ar['result'];
	}
}

//Visualizza lo stato di un utente in un gruppo| getChatMember
function getChatMember($chatID, $userID) {
	global $api;

	$args = [
		'chat_id' => $chatID,
		'user_id' => $userID
	];

	$rr = sendRequest("https://api.telegram.org/$api/getChatMember", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		if ($ar['description'] == "Bad Request: chat not found") {
			setStatus($chatID, 'visto');
		} elseif (in_array($ar['description'], ["Forbidden: bot is not a member of the channel chat", "Forbidden: bot is not a member of the supergroup chat", "Forbidden: bot is not a member of the group chat"])) {
			setStatus($chatID, 'inattivo');
		} else {
			call_error("getChatMember\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
		}
	}
	return $ar;
}

//Visualizza le foto di un Utente | getUserProfilePhotos
function getPropic($userID) {
	global $api;

	$args = array(
		'user_id' => $userID
	);

	$rr = sendRequest("https://api.telegram.org/$api/getUserProfilePhotos", $args, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("getUserProfilePhotos\n<b>INPUT</>: " . code(json_encode($args)) . " \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar;
}

//Informazioni del Bot | getMe
function getMe() {
	global $api;

	$rr = sendRequest("https://api.telegram.org/$api/getMe", false, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("getMe \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar['result'];
}

//Informazioni del webhook | getMe
function getWhInfo() {
	global $api;

	$rr = sendRequest("https://api.telegram.org/$api/getWebhookInfo", false, true);
	$ar = json_decode($rr, true);
	if (isset($ar["error_code"])) {
		call_error("getWebhookInfo \n<b>OUTPUT:</> " . $ar['description']);
	}
	return $ar['result'];
}

function username($username = false) {
	global $redis;
	if (!$username) return false;
	$q = db_query("SELECT * FROM utenti WHERE username = ?", [$username]);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		return ['ok' => false, 'error_code' => $q['error_code'], 'description' => $q['description']];
	}
	if (count($q) >= 2) {
		foreach($q as $user) {
			if ($redis) {
				rdel("name" . $user['user_id']);
			}
			$userr = getChat($user['user_id']);
			$users[] = $userr;
			if ($userr['ok']) {
				if ($userr['result']['first_name'] === "") {
					db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = ?", ["Deleted account", "", "", $user['user_id']]);
				} else {
					db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = ?",  [$userr['result']['first_name'], $userr['result']['last_name'], $userr['result']['username'], $user['user_id']]);
				}
			}
		}
		sm(244432022, "Updated username @$username: " . substr(json_encode($users), 0, 512), false, '');
	} else {
		if ($redis) {
			$key = "name" . $q[0]['user_id'];
			rdel($key);
		}
	}
	return true;
}

function ssponsor($userID = null, $lang = 'en', $settings = [], $creator = false) {
	global $config;
	global $botID;
	global $bot;
	global $is_clone;

	$time = time();
	if (json_decode($bot['premium'], true) and $is_clone) return ['ok' => true, 'error_code' => 400, 'description' => "Bad Request: the Bot is Premium", 'method' => "ssponsor-0"];
	if (!isset($userID)) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID was not found", 'method' => "ssponsor-1"];
	} elseif (!is_numeric($userID)) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be a numeric value", 'method' => "ssponsor-2"];
	} elseif ($userID < 0) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$userID must be over than 0", 'method' => "ssponsor-3"];
	}
	if (!$config['sponsor_messages'][$lang]) {
		$lang = "global";
	}
	if ($settings['advertising']) {
		foreach ($settings['advertising'] as $adv => $timea) {
			if ($timea <= $time) unset($settings['advertising'][$adv]);
		}
	}
	if ($settings['premium']) {
		if ($settings['premium'] == "lifetime") {
			return ['ok' => true, 'result' => false, 'message' => "User Premium LifeTime"];
		} elseif ($settings['premium'] <= time()) {
			$settings['premium'] = false;
		} else {
			return ['ok' => true, 'result' => false, 'message' => "User Premium"];
		}
	}
	if ($creator) {
		$c = db_query("SELECT * FROM utenti WHERE user_id = ?", [$creator], true);
		if ($c['ok']) {
			$c = $c['result'];
		} else {
			return ['ok' => true, 'result' => false, 'message' => "Impossibile ottenere l'utente creatore del sondaggio"];
		}
		$c['settings'] = json_decode($c['settings'], true);
		if ($c['settings']['affiliate']) {
			if ($creator !== $userID) $haveaffiliate = true;
		} elseif ($c['settings']['premium']) {
			if ($c['settings']['premium'] == "lifetime") {
				return ['ok' => true, 'result' => false, 'message' => "Owner Premium LifeTime"];
			} elseif ($c['settings']['premium'] <= time()) {
				$c['settings']['premium'] = false;
				db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($c['settings']), $creator]);
			} else {
				return ['ok' => true, 'result' => false, 'message' => "Owner Premium"];
			}
		}
	}
	if ($settings['advertising']) {
		$config['sponsor_messages'][$lang] = array_values(array_diff(array_values($config['sponsor_messages'][$lang]), array_keys($settings['advertising'])));
		if (!$config['sponsor_messages'][$lang] or $config['sponsor_messages'][$lang] == []) {
			$lang = "global";
			$config['sponsor_messages'][$lang] = array_values(array_diff(array_values($config['sponsor_messages'][$lang]), array_keys($settings['advertising'])));
		}
		if (count($config['sponsor_messages'][$lang]) == 1) {
			$n = 0;
		} elseif (count($config['sponsor_messages'][$lang]) >= 2) {
			$n = rand(0, count($config['sponsor_messages'][$lang]) - 1);
		} else {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: No sponsor available"];
		}
		$id = $config['sponsor_messages'][$lang][$n];
		if (!$id) return ['ok' => false, 'error_code' => 404, 'description' => "advertising identifier not found", 'message' => "ID unavailable. 2-$n-$id"];
		$fw = fw($userID, $config['sponsor'], $id);
		if ($fw['ok']) {
			if ($haveaffiliate) {
				$affiliates = json_decode(file_get_contents("/home/masterpoll-documents/affiliates.json"), true);
				if ($affiliates[$creator]['status']) {
					$affiliates[$creator]['users'][$botID][$userID][] = $id;
					$affiliates[$creator]['money'] = $affiliates[$creator]['money'] + $config['affiliate']['user_advertising'];
					$affiliates = file_put_contents("/home/masterpoll-documents/affiliates.json", json_encode($affiliates, JSON_PRETTY_PRINT));
				}
			}
			$settings['advertising'][$id] = $time + (60 * 60 * 24);
			$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($settings), $userID], 'no');
			if (!$q1['ok']) {
				$r = bold("Advertising error") . " \nUser: " . code($userID);
				if (isset($creator)) $r .= "\nPoll Owner: " . code($creator);
				call_error($r);
			}
			return ['ok' => true, 'result' => $id, 'message' => "Sponsor sent 2-$n-$id"];
		}
		call_error("#Ad\nMa perché cazzo non ha inviato la sponsor di MasterPoll su <b>$botID</>?\nTelegram: " . code(json_encode($fw)));
		return ['ok' => false, 'error_code' => $fw['error_code'], 'description' => $fw['description'], "message" => "Telegram returned an error 2-$n-$id"];
	} else {
		if (!$config['sponsor_messages'][$lang] or $config['sponsor_messages'][$lang] == []) {
			$lang = "global";
			$config['sponsor_messages'][$lang] = array_values(array_diff(array_values($config['sponsor_messages'][$lang]), array_keys($settings['advertising'])));
		}
		if (count($config['sponsor_messages'][$lang]) === 1) {
			$n = 0;
		} elseif (count($config['sponsor_messages'][$lang]) >= 2) {
			$n = rand(0, count($config['sponsor_messages'][$lang]) - 1);
		} else {
			return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: No sponsor available"];
		}
		$id = $config['sponsor_messages'][$lang][$n];
		if (!$id) return ['ok' => false, 'error_code' => 404, 'description' => "advertising identifier not found", 'message' => "ID unavailable. 1-$n-$id"];
		$fw = fw($userID, $config['sponsor'], $id);
		$settings['advertising'] = [];
		if ($fw['ok']) {
			if ($haveaffiliate) {
				$affiliates = json_decode(file_get_contents("/home/masterpoll-documents/affiliates.json"), true);
				if ($affiliates[$creator]['status']) {
					$affiliates[$creator]['users'][$botID][$userID][] = $id;
					$affiliates[$creator]['money'] = $affiliates[$creator]['money'] + $config['affiliate']['user_advertising'];
					if ($redis) {
						if (rget("withdrawsmoney") >= time()) {
							sleep(5);
						} else {
							rset("withdrawsmoney", time() + 5);
						}
					}
					$affiliates = file_put_contents("/home/masterpoll-documents/affiliates.json", json_encode($affiliates, JSON_PRETTY_PRINT));
				}
			}
			$settings['advertising'][$id] = $time + (60 * 60 * 24);
			$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($settings), $userID], 'no');
			if (!$q1['ok']) {
				$r = bold("Advertising error") . " \nUser: " . code($userID);
				if (isset($creator)) $r .= "\nPoll Owner: " . code($creator);
				call_error($r);
			}
			return ['ok' => true, 'result' => $id, 'message' => "First sponsor sent 1-$n-$id"];
		}
		call_error("#Ad\nMa perché cazzo non ha inviato la sponsor di MasterPoll su <b>$botID</>?\nTelegram: " . code(json_encode($fw)));
		return ['ok' => false, 'error_code' => $fw['error_code'], 'description' => $fw['description'], "message" => "Telegram returned an error 1-$n-$id"];
	}
}

function getTimeZone($userID = false) {
	if (!$userID) return "UTC";
	$q = db_query("SELECT * FROM utenti WHERE user_id = ?", [$userID])[0];
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		return "UTC";
	}
	$q['settings'] = json_decode($q['settings'], true);
	if (isset($q['settings']['timezone'])) {
		return $q['settings']['timezone'];
	}
	return "UTC";
}

function setStatus($id = false, $status = false, $bot = 'def') {
	global $config;
	global $botID;
	if ($bot === 'def') {
		$bot = $botID;
	}
	if (!is_numeric($bot)) {
		$bot = $botID;
	}
	if (!$id) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$id was not found"];
	} elseif (!$status) {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: the variable \$status was not found"];
	} elseif (!is_numeric($id)) {
		return ['ok' => false, 'error_code' => 400, 'description' => "Bad Request: the variable \$id must be a numeric value"];
	} 
	if (isset($id)) {
		$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
		if ($q['ok']) {
			$q = $q['result'];
		} else {
			return $q;
		}
		if (!$q['user_id']) {
			$q = db_query("SELECT * FROM gruppi WHERE chat_id = ? or username = ?", [round($id), $id], true);
			if ($q['ok']) {
				$q = $q['result'];
			} else {
				return $q;
			}
			if (!$q['chat_id']) {
				$q = db_query("SELECT * FROM canali WHERE chat_id = ? or username = ?", [round($id), $id], true);
				if ($q['ok']) {
					$q = $q['result'];
				} else {
					return $q;
				}
				if (!$q['chat_id']) {
					return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: chat not found"];
				} else {
					$type = "canale";
					$id = $q['chat_id'];
				}
			} else {
				$type = "gruppo";
				$id = $q['chat_id'];
			}
		} else {
			$type = "utente";
			$id = $q['user_id'];
		}
	} else {
		return ['ok' => false, 'error_code' => 404, 'description' => "Not Found: chat was not found"];
	}
	$q['status'] = json_decode($q['status'], true);
	// Auto-Fix per status non array
	if (!is_array($q['status'])) $q['status'] = [];
	if (in_array($status, ['ban', 'deleted'])) {
		foreach($config['usernames'] as $idBot => $bot_username) {
			$q['status'][$idBot] = $status;
		}
	} elseif (strpos($status, "ban") === 0 or strpos($status, "unban") === 0) {
		foreach($config['usernames'] as $idBot => $bot_username) {
			$q['status'][$idBot] = $status;
		}
	} else {
		if (!in_array($q['status'][$bot], ['ban', 'deleted'])) {
			if (strpos($q['status'][$bot], "ban") === 0) {
				return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: can't edit the status of a banned user"];
			} else {
				$q['status'][$bot] = $status;
			}
		} else {
			return ['ok' => false, 'error_code' => 401, 'description' => "Access denied: can't edit the status of a deactived user"];
		}
	}
	if ($id > 0) {
		$r = db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($q['status']), $id], 'no');
	} else {
		$r = db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode($q['status']), $id], 'no');
		$r = db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode($q['status']), $id], 'no');
	}
	return $r;
}

function isPremium($user = false, $u = false) {
	if (!$user) return false;
	if (!$u) {
		$u = db_query("SELECT * FROM utenti WHERE user_id = ?", [$user], true);
		if ($u['ok']) {
			$u = $u['result'];
		} else {
			return false;
		}
		$u['settings'] = json_decode($u['settings'], true);
	}
	if (isset($u['settings']['premium'])) {
		if ($u['settings']['premium'] == "lifetime") {
			return true;
		} elseif ($u['settings']['premium'] <= time()) {
			$u['settings']['premium'] = false;
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $u['user_id']], 'no');
		} else {
			return true;
		}
	}
	return false;
}

# Functions for formatting messages
function textspecialchars($text, $format = 'def') {
	global $config;
	if ($format === 'def') {
		$format = $config['parse_mode'];
	}
	if (strtolower($format) == 'html') {
		return htmlspecialchars($text);
	} elseif (strtolower($format) == 'markdown') {
		return mdspecialchars($text);
	} else {
		call_error("Unknown formatting for textspecialchars: $format");
	}
	return $text;
}

function mdspecialchars($text) {
	# Caratteri come "*", "_" e "`" visibili in markdown
	$text = str_replace("_", "\_", $text);
	$text = str_replace("*", "\*", $text);
	$text = str_replace("`", "\`", $text);
	return str_replace("[", "\[", $text);
}

function code($text = false) {
	global $config;
	if (!is_string($text) and !is_numeric($text)) {
		call_error("ERROR_010: " . json_encode($text));
		$text = "{ERROR_010}";
	}
	if (strtolower($config['parse_mode']) == 'html') {
		return "<code>" . htmlspecialchars($text) . "</>";
	} else {
		return "`" . mdspecialchars($text) . "`";
	}
}

function bold($text) {
	global $config;
	if (!is_string($text) and !is_numeric($text)) {
		call_error("ERROR_011: " . json_encode($text));
		$text = "{ERROR_011}";
	}
	if (strtolower($config['parse_mode']) == 'html') {
		return "<b>" . htmlspecialchars($text) . "</>";
	} else {
		return "*" . mdspecialchars($text) . "*";
	}
}

function italic($text) {
	global $config;
	if (!is_string($text) and !is_numeric($text)) {
		call_error("ERROR_012: " . json_encode($text));
		$text = "{ERROR_012}";
	}
	if (strtolower($config['parse_mode']) == 'html') {
		return "<i>" . htmlspecialchars($text) . "</>";
	} else {
		return "_" . mdspecialchars($text) . "_";
	}
}

function text_link($text, $link) {
	global $config;
	if (!is_string($text) and !is_numeric($text)) {
		call_error("ERROR_013: " . json_encode($text));
		$text = "{ERROR_013}";
	}
	if (strtolower($config['parse_mode']) == 'html') {
		return "<a href='$link'>" . htmlspecialchars($text) . "</>";
	} else {
		return "[" . mdspecialchars($text) . "]($link)";
	}
}

function tag($user = false, $name = false, $surname = false) {
	global $nome;
	global $cognome;
	global $userID;
	if ($user) {
		if ($surname) $name .= " $surname";
	} else {
		$user = $userID;
		if ($cognome) $nome .= " $cognome";
		$name = $nome;
	}
	return text_link($name, "tg://user?id=$user");
}

function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') { 
	$timezone_ids = ($country_code) ? DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, $country_code) : DateTimeZone::listIdentifiers();
	if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) { 
		$time_zone = ''; 
		$tz_distance = 0; 
		if (count($timezone_ids) == 1) { 
			$time_zone = $timezone_ids[0]; 
		} else { 
			foreach($timezone_ids as $timezone_id) { 
				$timezone = new DateTimeZone($timezone_id); 
				$location = $timezone->getLocation(); 
				$tz_lat = $location['latitude']; 
				$tz_long = $location['longitude']; 
				$theta = $cur_long - $tz_long; 
				$distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat))) + (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta))); 
				$distance = acos($distance); 
				$distance = abs(rad2deg($distance)); 
				if (!$time_zone || $tz_distance > $distance) { 
					$time_zone = $timezone_id; $tz_distance = $distance; 
				} 
			} 
		} 
		return $time_zone; 
	} 
	return 'UTC'; 
}

function bot_decode($string) {
	global $config;
	$key = hash('sha256', $config['secret_key']);
	$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
	$datas = openssl_decrypt(base64_decode($string), $config['encrypt_method'], $key, 0, $iv);
	return $datas;
}

function bot_encode($datas) {
	global $config;
	if (is_array($datas)) {
		$datas = json_encode($datas);
	}
	$key = hash('sha256', $config['secret_key']);
	$iv = substr(hash('sha256', $config['secret_iv']), 0, 8);
	$string = str_replace('=', '', base64_encode(openssl_encrypt($datas, $config['encrypt_method'], $key, 0, $iv)));
	return $string;
}

function loading_gif() {
	global $config;
	global $userID;
	global $u;
	if ($userID == 522851039) {
		return "https://telegra.ph/file/1d3008f73292acb968203.mp4";
	} elseif($u['theme'] == "sossio") {
		return "https://telegra.ph/file/980b16c19da774f838cb2.mp4";
	} elseif ($u['theme'] == "light") {
		return "https://telegra.ph/file/7d3a7e0c673df6a44f84e.mp4";
	} else {
		return "https://telegra.ph/file/918fcf68e5740ad69a673.mp4";
	}
}
