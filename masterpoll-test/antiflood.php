<?php

/*
NeleBotFramework
	Copyright (C) 2018  PHP-Coders

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

# Conigurazioni Anti-Flood Bot
$antiflood = [
	'messaggi' => 50,
	// Numero di messaggi che un utente può mandare entro i secondi per poi essere punito
	'secondi' => 2,
	// Secondi disponibili per inviare il numero di messaggi per poi essere punito
	'punizione' => 60 * 60 * 12, //Questi sono mezz'ora
	// Punizioni disponibili: 'forever' è per sempre oppure inserisci il tempo di ban in secondi
	'ban-message' => "",
	// Messaggio di Ban
	'unban-message' => "",
	// Messaggio di UnBan
];

require($f['plugins.dir'] . "/secure_mode.php");

# Anti-Flood solo per utenti senza permessi da Amministratore, gruppi e canali
if ($update) {
	$time = time();
	if ($typechat == "private") {
		if ($get = $redis->get($userID)) {
			$json = json_decode($get, true);
			if (isset($json['ban'])) {
				die;
			}
			if (date('s', time() - $json['tempo']) >= $antiflood['secondi']) {
				$json = [
					'tempo' => $time,
					'messaggi' => 1
				];
				$redis->set($userID, json_encode($json));
			} else {
				$json['messaggi'] = $json['messaggi'] + 1;
				$json['tempo'] = $time;
				if ($json['messaggi'] >= $antiflood['messaggi']) {
					$json['ban'] = true;
					$banFromAntiflood = $antiflood['punizione'];
				}
				$redis->set($userID, json_encode($json));
			}
		} else {
			$json = [
				'tempo' => $time,
				'messaggi' => 1
			];
			$redis->set($userID, json_encode($json));
		}
	} elseif (in_array($typechat, ['channel', 'supergroup', 'group'])) {
		if ($get = $redis->get($chatID)) {
			$json = json_decode($get, true);
			if (isset($json['ban'])) {
				$config['console'] = false;
				lc($chatID);
				die;
			}
			if (date('s', time() - $json['tempo']) >= $antiflood['secondi']) {
				$json = [
					'tempo' => $time,
					'messaggi' => 1
				];
				$redis->set($chatID, json_encode($json));
			} else {
				$json['messaggi'] = $json['messaggi'] + 1;
				$json['tempo'] = $time;
				if ($json['messaggi'] >= $antiflood['messaggi']) {
					$json['ban'] = true;
					$banFromAntiflood = $antiflood['punizione'];
				}
				$redis->set($chatID, json_encode($json));
			}
		} else {
			$json = [
				'tempo' => $time,
				'messaggi' => 1
			];
			$redis->set($chatID, json_encode($json));
		}
	}
	if ($cmd == "lascia") {
		rdel("updates-$botID");
		die;
	}
	$get_updates = rget("updates-$botID");
	if ($get_updates['ok']) {
		if (is_numeric($get_updates['result'])) {
			usleep(1000);
			if (rget("updates-$botID")['result'] > $update['update_id']) {
				$config['json_payload'] = true;
				if (in_array($typechat, ['group', 'supergroup', 'channel'])) {
					lc($chatID);
				} elseif ($cbdata) {
					cb_reply($cbid, '⚠️ Please, try again later', false);
				} elseif ($typechat == "private") {
					sm($chatID, "⚠️ Please, try again later");
				}
				die;
			}
		} else {
			rset("updates-$botID", $update['update_id']);
		}
	} else {
		if ($cbdata) {
			cb_reply($cbid, '⚠️ Please, try again later', false);
		} elseif ($typechat == "private") {
			sm($chatID, "⚠️ Please, try again later");
		}
		call_error("Errore redis: " . code(json_encode($get_updates)));
		die;
	}
}
