<?php

# Connection to Redis
$times['redis'] = microtime(true);
if ($config['usa_redis']) {
	$redisc = $config['redis'];
	if (!isset($redis)) {
		try {
			$redis = new Redis();
			$redis->connect($redisc['host'], $redisc['port']);
		} catch (Exception $e) {
			$config['json_payload'] = true;
			$config['response'] = false;
			if ($cbdata) {
				cb_reply($cbid, '❌ Please try again later...', true);
			} elseif ($chatID == $userID) {
				sm($chatID, "❌ Please try again later...");
			}
			call_error($e->getMessage(), $f['database']);
			die;
		}
		if ($redisc['password'] !== false) {
			try {
				$redis->auth($redisc['password']);
			} catch (Exception $e) {
				call_error($e->getMessage(), $f['database']);
				die;
			}
		}
		if ($redisc['database'] !== false) {
			try {
				$redis->select($redisc['database']);
			} catch (Exception $e) {
				call_error($e->getMessage(), $f['database']);
				die;
			}
		}
	}
	if ($config['usa_il_db']) require($f['anti-flood']);
	$rget = rget($botID . "-" . $update['update_id']);
	if (!$rget['ok']) {
		call_error("Errore redis: " . json_encode($rget));
		die;
	}
	if ($rget['result']) {
		if (!$is_clone) {
			$config['json_payload'] = false;
			$config['response'] = false;
			file_put_contents("update-" . $update['update_id'] . ".json", json_encode($rupdate, JSON_PRETTY_PRINT));
			$bot = getWhInfo();
			sd($config['console'], "update-" . $update['update_id'] . ".json", "L'Update " . $update['update_id'] . " è stata ripetuta! \nTelegram at " . date("c", $bot['last_error_date']) . ": " . $bot['last_error_message'] . "\nBot: @" . $config['username_bot'] . " [" . code($botID) . "]");
			sleep(1);
			if (file_exists("update-" . $update['update_id'] . ".json")) unlink("update-" . $update['update_id'] . ".json");
		}
		die;
	} else {
		rset($botID . "-" . $update['update_id'], true);
	}
	if ($cmd == "redis" and $isadmin) {
		$redisping_start = microtime(true);
		$test = $redis->ping();
		$redisping_end = microtime(true);
		if ($test == "+PONG") {
			$res = "✅";
		} else {
			$res = "⚠";
		}
		sm($chatID, bold("Risultato del Test: ") . $res . "\n<b>Ping:</b> " . number_format($redisping_end - $redisping_start, 10));
		die;
	}
	if ($cmd == "redis_del" and $isadmin) {
		$test = $f['languages'];
		$rdel = rdel($test);
		if ($rdel['result']) {
			$res = "✅";
		} else {
			$res = "⚠";
		}
		sm($chatID, bold("Risultato del del: ") . $res);
		die;
	}
} else {
	if ($cmd == "redis" and $isadmin) {
		sm($chatID, bold("Risultato del Test: ") . "❌\nRedis is deactivated! Activate it from the config file.");
	}
	die;
}

# Connessione al Database
$times['postgresql'] = microtime(true);
if ($config['usa_il_db']) {
	if (strtolower($database['type']) == 'sqlite') {
		try {
			$PDO = new PDO("sqlite:" . $database['nome_database']);
		} catch (PDOException $e) {
			call_error($e->getMessage(), ['pdo', 'database'], $f['database']);
			die;
		}
		$query = "CREATE TABLE IF NOT EXISTS utenti (
		user_id INT(20) NOT NULL ,
		nome VARCHAR(64) NOT NULL ,
		cognome VARCHAR(64) ,
		username VARCHAR(32) ,
		lang VARCHAR(10) NOT NULL ,
		page VARCHAR(512) ,
		settings VARCHAR(4096) DEFAULT '[]',
		banlist VARCHAR(4096) DEFAULT '[]',
		first_update VARCHAR(64)	NOT NULL ,
		last_update VARCHAR(64)	NOT NULL ,
		status VARCHAR(4096)	DEFAULT '[]');";
		$PDO->query($query);
		$err = $PDO->errorInfo();
		if ($err[0] !== "00000") {
			call_error("PDO Error: errore nella creazione della tabella utenti, OUTPUT: " . json_encode($err), ['pdo', 'database'], $f['database']);
			die;
		}
		$query = "CREATE TABLE IF NOT EXISTS gruppi (
		chat_id BIGINT(20) NOT NULL ,
		title VARCHAR(64) NOT NULL ,
		description VARCHAR(256) ,
		username VARCHAR(32) ,
		admins VARCHAR(4096) DEFAULT '[]',
		permissions VARCHAR(1024) DEFAULT '[]',
		page VARCHAR(512) ,
		status VARCHAR(1024) DEFAULT '[]');";
		$PDO->query($query);
		$err = $PDO->errorInfo();
		if ($err[0] !== "00000") {
			call_error("PDO Error: errore nella creazione della tabella gruppi, OUTPUT: " . json_encode($err), ['pdo', 'database'], $f['database']);
			die;
		}
		if ($config['post_canali']) {
			$query = "CREATE TABLE IF NOT EXISTS canali (
			chat_id BIGINT(20) NOT NULL ,
			title VARCHAR(64) NOT NULL ,
			description VARCHAR(256) ,
			username VARCHAR(32) ,
			admins VARCHAR(4096) DEFAULT '[]' ,
			page VARCHAR(512) ,
			status VARCHAR(50) DEFAULT '[]');";
			$PDO->query($query);
			$err = $PDO->errorInfo();
			if ($err[0] !== "00000") {
				call_error("PDO Error: errore nella creazione della tabella canali, OUTPUT: " . json_encode($err), ['pdo', 'database'], $f['database']);
				die;
			}
		}
	} elseif (strtolower($database['type']) == 'postgre') {
		try {
			$PDO = new PDO("pgsql:host=" . $database['host'] . ";dbname=" . $database['nome_database'], $database['utente'], $database['password']);
		} catch (PDOException $e) {
			$config['json_payload'] = true;
			$config['response'] = false;
			if ($cbdata) {
				cb_reply($cbid, '❌ Please try again later...', true);
			} elseif ($chatID == $userID) {
				sm($chatID, "❌ Please try again later...");
			}
			call_error($e->getMessage(), $f['database']);
			die;
		}
	} else {
		call_error("<b>Error:</b> this Bot only supports postgre as a database at the moment!", $f['database']);
		die;
	}
	
	if ($isadmin and $cmd == $database['type']) {
		$dbping_start = microtime(true);
		$test = db_query("SELECT user_id FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
		$dbping_end = microtime(true);
		if ($test['ok']) {
			$res = "✅";
		} else {
			$res = "⚠";
		}
		sm($chatID, bold("Risultato del Test: ") . $res . "\n<b>Ping:</b> " . number_format($dbping_end - $dbping_start, 10));
		die;
	}
	
	$c = db_query("SELECT count(*) FROM pg_stat_activity;");
	if ($c['ok']) {
		$c = $c['result'][0]['count'];
		if ($c > 30) {
			rset('secure_mode', time() + 5);
			//sm(244432022, "Connessioni alte: $c");
		}
		if ($cmd == "connections" and $isadmin) {
			sm($chatID, "Conn: $c");
			die;
		}
	}
	
	# Database di Utenti
	if ($userID) {
		$test_db_start = microtime(true);
		$u = db_query("SELECT * FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
		$test_db_end = microtime(true);
		if (($test_db_end - $test_db_start) >= 1) {
			rset('secure_mode', time() + 5);
			$secure_mode = true;
			require($f['plugins.dir'] . "/secure_mode.php");
		}
		if ($u['ok']) {
			$u = $u['result'];
		} else {
			$config['json_payload'] = true;
			$config['response'] = false;
			if ($cbdata) {
				cb_reply($cbid, "⚠️ Bot Error Auto-Reported", false);
			} elseif ($typechat == "private") {
				sm($chatID, "⚠️ Bot Error Auto-Reported");
			}
			call_error("#PDOError SELECT\n<b>User:</> $userID\n<b>Error code:</> " . $u['error_code'] . "\n<b>Error message:</> " . $u['description']);
			die;
		}
		if (!$cognome) {
			$cognome = "";
		}
		if (!$username) {
			$username = "";
		} else {
			$hausername = "\n<b>Username:</> @$username";
		}
		if (!isset($lingua)) {
			$lingua = "en";
		} else {
			$lingua = explode("-", $lingua)[0];
			$rest = [
				'root'	=> 'en',
				'eng'	=> 'en',
				'und'	=> 'en',
				'rus'	=> 'ru',
				'zh'	=> 'zh_TW',
				'yue'	=> 'zh_TW',
				'nob'	=> 'nb',
				'nb_NO'	=> 'no',
				'gsw'	=> 'de',
				'ita'	=> 'it',
				'uzb'	=> 'uz',
				'fas'	=> 'fa'
			];
			if (in_array($lingua, array_keys($rest))) {
				$lingua = $rest[$lingua];
			}
		}
		if (!$u) {
			$is_new = true;
			if ($redis->get("insert-$userID")) {
				die;
			} else {
				$redis->set("insert-$userID", true);
			}
			db_query("INSERT INTO utenti (user_id, nome, cognome, username, lang, page, status, last_update, first_update) VALUES (?,?,?,?,?,?,?,?,?)", [$userID, $nome, $cognome, $username, $lingua, '', json_encode([$botID => "attivo"]), time(), time()], "no");
			$u = db_query("SELECT * FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
			if ($u['ok']) {
				$u = $u['result'];
			} else {
				$config['json_payload'] = true;
				$config['response'] = false;
				if ($cbdata) {
					cb_reply($cbid, "⚠️ Bot Error Auto-Reported", false);
				} elseif ($typechat == "private") {
					sm($chatID, "⚠️ Bot Error Auto-Reported");
				}
				call_error("#PDOError INSERT \n<b>User:</> $userID\n<b>Error code:</> " . $u['error_code'] . "\n<b>Error message:</> " . $u['description']);
				die;
			}
		}
		if ($u) {
			# Ban dal Bot
			if (is_int($banFromAntiflood) and strpos($u['status'][array_keys($config['usernames'])[0]], "ban") !== 0) {
				$dateban = time() + $banFromAntiflood;
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($new), $userID], "no");
				sm($config['console'], "#Ban #AntiFlood #id$userID \n<b>Utente:</> " . tag() . " [". code($userID) . "] \n<b>Durata:</> " . date("d/m/Y h:i:s") . "\n@" . $config['username_bot']);
				die;
			}
			if ($nome !== $u['nome'] or $cognome !== $u['cognome'] or $username !== $u['username']) {
				if ($redis) {
					rdel("name$userID");
				}
				$u['nome'] = $nome;
				$u['cognome'] = $cognome;
				$u['username'] = $username;
				db_query("UPDATE utenti SET nome = ?, cognome = ?, username = ? WHERE user_id = ?", [$nome, $cognome, $username, $userID], 'no');
			}
			$u['settings'] = json_decode($u['settings'], true);
			if (!is_array($u['settings'])) {
				$u['settings'] = [];
				$update_settings = true;
			}
			$u['banlist'] = json_decode($u['banlist'], true);
			if (!isset($u['banlist'])) {
				$u['banlist'] = [];
				db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($u['banlist']), $userID], "no");
			}
			$u['web_devices'] = json_decode($u['web_devices'], true);
			if (!isset($u['web_devices'])) {
				$u['web_devices'] = [];
				db_query("UPDATE utenti SET web_devices = ? WHERE user_id = ?", [json_encode($u['web_devices']), $userID], "no");
			}
			$u['status'] = json_decode($u['status'], true);
			if (!$u['status'] or !is_array($u['status'])) {
				$u['status'] = [$botID => 'attivo'];
				db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID], "no");
			}
			if ($typechat == "private" and $u['status'][$botID] !== "ban") {
				if (in_array($u['status'][$botID], ["attivo", "wait", "inattesa", "blocked"])) {
					$ref_avviato = true;
					$u['status'][$botID] = "avviato";
					db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID], "no");
				}
			}
			if (!isset($u['settings']['timezone'])) {
				$u['settings']['timezone'] = "UTC";
				$update_settings = true;
			}
			if (!isset($u['settings']['date_format'])) {
				$u['settings']['date_format'] = "D, j M Y H:i";
				$update_settings = true;
			}
			if (isset($u['settings']['premium']) and $u['settings']['premium'] !== "lifetime") {
				if ($u['settings']['premium'] < time()) {
					unset($u['settings']['premium']);
					$update_settings = true;
				}
			}
			if (!$u['settings']['type']) {
				$u['settings']['type'] = "vote";
				$u['settings']['anonymous'] = true;
				$update_settings = true;
			} else {
				if (in_array($u['settings']['type'], array_keys($config['types']))) {
					if (!$config['types'][$u['settings']['type']]) {
						$toglitype = true;
					}
				} else {
					$toglitype = true;
				}
				if ($toglitype) {
					foreach ($config['types'] as $type => $act) {
						if ($act and !$fine) {
							$thistype = $type;
							$fine = true;
						}
					}
					if ($thistype) {
						$u['settings']['type'] = $thistype;
						$update_settings = true;
					} else {
						$update_settings = false;
					}
				}
			}
			if (isset($update_settings)) {
				db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
			}
			if ($u['last_update'] + 60 < time() and $u['status'][$botID] !== "ban") {
				$u['last_update'] = time();
				db_query("UPDATE utenti SET last_update = ? WHERE user_id = ?", [$u['last_update'], $userID], "no");
			} else {
				//$config['json_payload'] = true;
			}
		} else {
			call_error("Utente non caricato sul db: $userID");
			die;
		}
		date_default_timezone_set($u['settings']['timezone']);
	}
	
	# Datbase di Gruppi e Canali
	if ($chatID < 0) {
		if ($typechat == "supergroup") {
			if ($chatbanFromAntiflood) {
				$dateban = time() + $chatbanFromAntiflood;
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				db_query("UPDATE gruppi SET status = ? WHERE chat_id = ? LIMIT 1", [json_encode($new), $chatID], "no");
				die;
			}
			$g = db_query("SELECT * FROM gruppi WHERE chat_id = ?", [$chatID], true);
			if ($g['ok']) {
				$g = $g['result'];
			} else {
				$config['json_payload'] = true;
				$config['response'] = false;
				call_error("#PDOError \n<b>Group:</> $chatID\n<b>Error code:</> " . $g['error_code'] . "\n<b>Error message:</> " . $g['description']);
				die;
			}
			if (!$g) {
				if (!isset($usernamechat)) {
					$usernamechat = "";
				}
				$getchat = getChat($chatID);
				if (isset($getchat['result']['permissions'])) {
					$perms = $getchat['result']['permissions'];
				} else {
					$perms = ["can_send_messages" => true, "can_send_media_messages" => true, "can_send_polls" => true, "can_send_other_messages" => true, "can_add_web_page_previews" => true, "can_change_info" => false, "can_invite_users" => false, "can_pin_messages" => false];
				}
				$g['permissions'] = json_encode($perms);
				if (isset($getchat['result']['description'])) {
					$descrizione = $getchat['result']['description'];
				} else {
					$descrizione = "";
				}
				$admins = getAdmins($chatID);
				if (isset($admins['ok'])) {
					$adminsg = json_encode($admins['result']);
				} else {
					$adminsg = "[]";
				}
				db_query("INSERT INTO gruppi (chat_id, title, description, username, admins, page, permissions, status) VALUES (?,?,?,?,?,?,?,?)", [$chatID, $title, $descrizione, $usernamechat, $adminsg, '', json_encode($g['permissions']), json_encode([$botID => "attivo"])], "no");
				$g = db_query("SELECT * FROM gruppi WHERE chat_id = ? LIMIT 1", [$chatID], true);
				if ($g['ok']) {
					$g = $g['result'];
				} else {
					$config['json_payload'] = true;
					$config['response'] = false;
					call_error("#PDOError \n<b>Group:</> $chatID\n<b>Error code:</> " . $g['error_code'] . "\n<b>Error message:</> " . $g['description']);
					die;
				}
			}
			if ($g) {
				$g['status'] = json_decode($g['status'], true);
				if (!$g['status'] or !is_array($g['status'])) $g['status'] = [];
				if (!isset($usernamechat)) {
					$usernamechat = "";
				}
				// Aggiornamento automatico della chat
				if ($title !== $g['title'] or $usernamechat !== $g['username']) {
					$getchat = getChat($chatID);
					if (isset($getchat['result']['permissions'])) {
						$perms = $getchat['result']['permissions'];
					} else {
						$perms = ["can_send_messages" => true, "can_send_media_messages" => true, "can_send_polls" => true, "can_send_other_messages" => true, "can_add_web_page_previews" => true, "can_change_info" => false, "can_invite_users" => false, "can_pin_messages" => false];
					}
					$g['permissions'] = json_encode($perms);
					if (isset($getchat['result']['description'])) {
						$descrizione = $getchat['result']['description'];				
					} else {
						$descrizione = "";
					}
					$admins = getAdmins($chatID);
					if (isset($admins['ok'])) {
						$adminsg = json_encode($admins['result']);
					} else {
						$adminsg = "[]";
					}
					db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ?, permissions = ?, page = ? WHERE chat_id = ?", [$title, $usernamechat, $adminsg, $descrizione, $g['permissions'], " ", $chatID], "no");
				}
				$g['permissions'] = json_decode($g['permissions'], true);
				// Auto-Fix per correggere l'array mancante o il json errato
				if (!is_array($g['permissions'])) {
					$g['permissions'] = ["can_send_messages" => true, "can_send_media_messages" => true, "can_send_polls" => true, "can_send_other_messages" => true, "can_add_web_page_previews" => true, "can_change_info" => false, "can_invite_users" => false, "can_pin_messages" => false];
					db_query("UPDATE gruppi SET permissions = ? WHERE chat_id = ?", [json_encode($g['permissions']), $chatID], "no");
				}
				// Attivazione della chat
				if (in_array($g['status'][$botID], ["inattivo", "inattesa"])) {
					if (in_array($typechat, ["group", "supergroup"])) {
						$g['status'][$botID] = 'avviato';
						db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode($q['status']), $chatID], "no");
					} else {
						$g['status'][$botID] = 'attivo';
						db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode($q['status']), $chatID], "no");
					}
				}
			}	
		} elseif ($typechat == "channel") {
			if (is_int($chatbanFromAntiflood)) {
				$dateban = time() + $chatbanFromAntiflood;
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				db_query("UPDATE canali SET status = ? WHERE chat_id = ? LIMIT 1", [json_encode($new), $chatID], "no");
				die;
			}
			$c = db_query("SELECT * FROM canali WHERE chat_id = ?", [$chatID], true);
			if ($c['ok']) {
				$c = $c['result'];
			} else {
				$config['json_payload'] = true;
				$config['response'] = false;
				call_error("#PDOError \n<b>Channel:</> $chatID\n<b>Error code:</> " . $c['error_code'] . "\n<b>Error message:</> " . $c['description']);
				die;
			}
			if (!$c) {
				if (!isset($usernamechat)) {
					$usernamechat = "";
				}
				$getchat = getChat($chatID);
				if (isset($getchat['result']['description'])) {
					$descrizione = $getchat['result']['description'];
				} else {
					$descrizione = "";
				}
				$admins = getAdmins($chatID);
				if (isset($admins['ok'])) {
					$adminsg = json_encode($admins['result']);
				} else {
					$adminsg = "[]";
				}
				db_query("INSERT INTO canali (chat_id, title, description, username, admins, page, status) VALUES (?,?,?,?,?,?,?)", [$chatID, $title, $descrizione, $usernamechat, $adminsg, '', json_encode([$botID => 'attivo'])], 'no');
				$c = db_query("SELECT * FROM canali WHERE chat_id = ? LIMIT 1", [$chatID], true);
				if ($c['ok']) {
					$c = $c['result'];
				} else {
					$config['json_payload'] = true;
					$config['response'] = false;
					call_error("#PDOError \n<b>Group:</> $chatID\n<b>Error code:</> " . $c['error_code'] . "\n<b>Error message:</> " . $c['description']);
					die;
				}
			}
			if ($c) {
				$c['status'] = json_decode($c['status'], true);
				if (!$c['status'] or !is_array($c['status'])) $c['status'] = [];
				if (!isset($usernamechat)) {
					$usernamechat = "";
				}
				// Aggiornamento automatico dei dati del canale
				if ($title !== $c['title'] or $usernamechat !== $c['username']) {
					$getchat = getChat($chatID);
					if (isset($getchat['result']['description'])) {
						$descrizione = $getchat['result']['description'];				
					} else {
						$descrizione = "";
					}
					$admins = getAdmins($chatID);
					if (isset($admins['ok'])) {
						$adminsg = json_encode($admins['result']);
					} else {
						$adminsg = "[]";
					}
					db_query("UPDATE canali SET title = ?, username = ?, admins = ?, description = ?, page = ? WHERE chat_id = ?", [$title, $usernamechat, $adminsg, $descrizione, ' ', $chatID], "no");
				}
				// Attivazione della chat
				if (in_array($c['status'][$botID], ["inattivo", "inattesa"])) {
					if ($typechat == "channel") {
						$c['status'][$botID] = 'avviato';
						db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode($c['status']), $chatID], "no");
					} else {
						$c['status'][$botID] = 'attivo';
						db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode($c['status']), $chatID], "no");
					}
				}
			}	
		}
	}
	
	# Database commands only for Bot Administrators
	if($isadmin) {
		# Online users
		if ($cmd == "online_month" or $cbdata == "onc_month") {
			$config['json_payload'] = false;
			if ($cmd and !$cbid) {
				$m = sm($chatID, "Carico...");
				$cbmid = $m['result']['message_id'];
			} else {
				cb_reply($cbid, "Carico...", false);
			}
			$time = time();
			$res = db_query("SELECT last_update, lang FROM utenti WHERE last_update >= ? ORDER BY last_update DESC", [($time - 30 * 24 * 60 * 60)], false);
			if ($res['ok']) {
				$res = $res['result'];
			} else {
				editMsg($chatID, "Impossibile richiedere informazioni dal database in questo momento...", $cbmid);
				die;
			}
			$languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
			$languages_flag = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
			foreach ($res as $dati) {
				if ($dati['last_update'] > $time - 30 * 24 * 60 * 60) {
					$onlinen[$dati['lang']] += 1;
				}
			}
			$menu[] = [
				[
					"text" => "Aggiorna 🔄",
					"callback_data" => "onc_month"
				]
			];
			$testo = "<b>👥 Online del mese su</> @" . $config['username_bot'] . "\n\n";
			$tot = count($res);
			foreach ($onlinen as $slang => $count) {
				if (!isset($languages_name[$slang])) $languages_name[$slang] = "Unknown";
				if (!isset($languages_flag[$slang])) $languages_flag[$slang] = "🏳️";
				if ($languages_flag[$slang] !== $languages_name[$slang]){
					$flag = $languages_flag[$slang] . " ";
				} else {
					$flag = "";
				}
				$languages_name[$slang][0] = strtoupper($languages_name[$slang][0]);
				$languagesz[$count][] = $flag . $languages_name[$slang];
			}
			ksort($languagesz, SORT_NUMERIC);
			$languagesz = array_reverse($languagesz, true);
			foreach ($languagesz as $count => $ls) {
				foreach($ls as $slang) {
					$perc = round($count / $tot * 100) . "%";
					$testo .= italic($slang) . ": $count ($perc)\n";
				}
			}
			$testo .= "\n" . number_format($tot) . " utenti totali.\n\n";
			$testo .= italic("🔄 Aggiornato il " . date("d/m/Y") . " alle " . date("h:i:s"));
			$f = editMsg($chatID, $testo, $cbmid, $menu);
			die;
		}
		if ($cmd == "online" or $cbdata == "onc" or strpos($cmd, "online_") === 0 or strpos($cbdata, "onc_") === 0) {
			$config['json_payload'] = false;
			if ($cmd and !$cbid) {
				$m = sm($chatID, "Carico...");
				$cbmid = $m['result']['message_id'];
			} else {
				cb_reply($cbid, "Carico...", false);
			}
			$time = time();
			if (strpos($cmd, "online_") === 0) {
				$slang = str_replace('online_', '', $cmd);
				$cbdata = "onc_" . $slang;
				$res = db_query("SELECT last_update FROM utenti WHERE lang = ? ORDER BY last_update DESC", [$slang], false);
			} elseif (strpos($cbdata, "onc_") === 0) {
				$slang = str_replace('onc_', '', $cbdata);
				$res = db_query("SELECT last_update FROM utenti WHERE lang = ? ORDER BY last_update DESC", [$slang], false);
			} else {
				$cbdata = "onc";
				$res = db_query("SELECT last_update FROM utenti ORDER BY last_update DESC", false, false);
			}
			if ($res['ok']) {
				$res = $res['result'];
			} else {
				editMsg($chatID, "Impossibile richiedere informazioni dal database in questo momento...", $cbmid);
				die;
			}
			foreach ($res as $dati) {
				if ($dati['last_update'] > $time - 60) {
					$onlinen['1min'] = $onlinen['1min'] + 1;
				}
				if ($dati['last_update'] > $time - 60 * 60) {
					$onlinen['1h'] = $onlinen['1h'] + 1;
				}
				if ($dati['last_update'] > $time - 24 * 60 * 60) {
					$onlinen['24h'] = $onlinen['24h'] + 1;
				}
				if ($dati['last_update'] > $time - 30 * 24 * 60 * 60) {
					$onlinen['30d'] = $onlinen['30d'] + 1;
				}
			}
			$menu[] = [
				[
					"text" => "Aggiorna 🔄",
					"callback_data" => $cbdata
				],
			];
			$testo = "<b>👥 Iscritti online su</> @" . $config['username_bot'] . "\n";
			if ($slang) {
				$languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
				$languages_flag = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
				if (!isset($languages_name[$slang])) $languages_name[$slang] = "Unknown";
				if (!isset($languages_flag[$slang])) $languages_flag[$slang] = "🏳️";
				$languages_name[$slang][0] = strtoupper($languages_name[$slang][0]);
				$testo .= italic($languages_flag[$slang] . " " . $languages_name[$slang]) . "\n\n";
			} else {
				$testo .= italic("🌐 Global") . "\n\n";
			}
			$testo .= number_format($onlinen['1min']) . " utenti online nell'ultimo minuto\n";
			$testo .= number_format($onlinen['1h']) . " utenti online nell'ultima ora\n";
			$testo .= number_format($onlinen['24h']) . " utenti online nelle ultime 24 ore\n";
			$testo .= number_format($onlinen['30d']) . " utenti online negli ultimi 30 giorni\n";
			$testo .= number_format(count($res)) . " utenti totali.\n\n";
			$testo .= italic("🔄 Aggiornato il " . date("d/m/Y") . " alle " . date("h:i:s"));
			$f = editMsg($chatID, $testo, $cbmid, $menu);
			die;
		}
		# Nuovi utenti
		if ($cmd == "newusers" or $cbdata == "newonc" or strpos($cmd, "newusers_") === 0 or strpos($cbdata, "newonc_") === 0) {
			$config['json_payload'] = false;
			if ($cmd and !$cbid) {
				$m = sm($chatID, "Carico...");
				$cbmid = $m['result']['message_id'];
			} else {
				cb_reply($cbid, "Carico...", false);
			}
			$time = time();
			if (strpos($cmd, "newusers_") === 0) {
				$slang = str_replace('newusers_', '', $cmd);
				$cbdata = "onc_" . $slang;
				$res = db_query("SELECT first_update FROM utenti WHERE lang = ? ORDER BY first_update DESC", [$slang], false);
			} elseif (strpos($cbdata, "newonc_") === 0) {
				$slang = str_replace('newonc_', '', $cbdata);
				$res = db_query("SELECT first_update FROM utenti WHERE lang = ? ORDER BY first_update DESC", [$slang], false);
			} else {
				$cbdata = "newonc";
				$res = db_query("SELECT first_update FROM utenti ORDER BY first_update DESC", false, false);
			}
			if ($res['ok']) {
				$res = $res['result'];
			} else {
				editMsg($chatID, "Impossibile richiedere informazioni dal database in questo momento...", $cbmid);
				die;
			}
			foreach ($res as $dati) {
				if ($dati['first_update'] > $time - 60) {
					$onlinen['1min'] = $onlinen['1min'] + 1;
				}
				if ($dati['first_update'] > $time - 60 * 60) {
					$onlinen['1h'] = $onlinen['1h'] + 1;
				}
				if ($dati['first_update'] > $time - 24 * 60 * 60) {
					$onlinen['24h'] = $onlinen['24h'] + 1;
				}
				if ($dati['first_update'] > $time - 30 * 24 * 60 * 60) {
					$onlinen['30d'] = $onlinen['30d'] + 1;
				}
			}
			$menu[] = [
				[
					"text" => "Aggiorna 🔄",
					"callback_data" => $cbdata
				],
			];
			$testo = "<b>👥 Nuovi iscritti su</> @" . $config['username_bot'] . "\n";
			if ($slang) {
				$languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
				$languages_flag = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
				if (!isset($languages_name[$slang])) $languages_name[$slang] = "Unknown";
				if (!isset($languages_flag[$slang])) $languages_flag[$slang] = "🏳️";
				$languages_name[$slang][0] = strtoupper($languages_name[$slang][0]);
				$testo .= italic($languages_flag[$slang] . " " . $languages_name[$slang]) . "\n\n";
			} else {
				$testo .= italic("🌐 Global") . "\n\n";
			}
			$testo .= number_format($onlinen['1min']) . " utenti online nell'ultimo minuto\n";
			$testo .= number_format($onlinen['1h']) . " utenti online nell'ultima ora\n";
			$testo .= number_format($onlinen['24h']) . " utenti online nelle ultime 24 ore\n";
			$testo .= number_format($onlinen['30d']) . " utenti online negli ultimi 30 giorni\n";
			$testo .= number_format(count($res)) . " utenti totali.\n\n";
			$testo .= italic("🔄 Aggiornato il " . date("d/m/Y") . " alle " . date("h:i:s"));
			$f = editMsg($chatID, $testo, $cbmid, $menu);
			die;
		}
		# Command to ban a user from the Bot
		if (strpos($cmd, "bl_add ") === 0 and $typechat == "private") {
			$ex = explode(" ", $cmd, 3);
			$id = str_replace("@", '', $ex[1]);
			if (isset($id)) {
				if (isset($ex[2])) {
					$dat = $ex[2];
					if (strpos($dat, "/") !== false) {
						$exp = explode(" ", $dat);
						$data = explode("/", $exp[0]);
						if (isset($exp[1])) {
							$ora = explode(":", $exp[1]);
						} else {
							$ora = "000";
						}
						$dateban = mktime($ora[0], $ora[1], $ora[2], $data[1], $data[0], $data[2]);
						if ($dateban < time()) {
							sm($chatID, "Invalid date. \n It would be out of order to the " . date("d/m/Y", $dateban) . " at " . date("H:i:s", $dateban));
							die;
						}
					} elseif (strpos($dat, "for ") !== false) {
						$exp = strtolower(str_replace("for ", '', $dat));
						$dateban = strtotime($exp);
						if ($dateban <= time()) {
							sm($chatID, "Invalid date. \n It would be out of order to the " . date("d/m/Y", $dateban) . " at " . date("H:i:s", $dateban));
							die;
						}
					} else {
						sm($chatID, "Invalid date.");
						die;
					}
					$al = "al " . date("d/m/Y", $dateban) . " alle " . date("H:i:s", $dateban);
					$stato = 'ban' . $dateban;
				} else {
					$al = "indefinitely.";
					$stato = 'ban';
				}
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if (!$q['ok']) {
					sm($chatID, "Errore dal db");
					die;
				} else {
					$q = $q['result'];
				}
				if (!$q['user_id']) {
					sm($chatID, "User not found in the database...");
					die;
				} elseif ($q['status'][$botID] === "ban" and !$ex[2]) {
					sm($chatID, "This user is already banned...");
					die;
				} else {
					$id = $q['user_id'];
				}
				sm($chatID, "I have banned " . tag($id, getName($id)['result']) . " until $al");
				foreach (array_keys($config['usernames']) as $idBot) {
					$new[$idBot] = "ban$dateban";
				}
				db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($new), $id], "no");
				sm($config['console'], "#Ban #id$userID \n<b>Banned user:</> " . tag($id, getName($id)['result']) . " [" . code($id) . "] \n<b>Date:</> " . date("d/m/Y h:i:s") . "\n<b>Unban date:</> " . $al . "\n<b>Ban by:</> " .tag());
			}
			die;
		}
		# Command for unban a user from the Bot
		if (strpos($cmd, "bl_remove ") === 0 and $typechat == "private") {
			$ex = explode(" ", $cmd, 2);
			$id = str_replace("@", '', $ex[1]);
			if (isset($id)) {
				$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
				if (!$q['ok']) {
					sm($chatID, "Errore dal db");
					die;
				} else {
					$q = $q['result'];
				}
				if (!$q['user_id']) {
					sm($chatID, "User not found in the database...");
					die;
				} elseif (strpos($q['status'], "ban") === false) {
					sm($chatID, "This user is not banned...");
					die;
				} else {
					$id = $q['user_id'];
				}
				if ($config['redis']) {
					rdel($id);
				}
				db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode([$botID => 'attivo']), round($id)], "no");
				sm($chatID, "I've unbanned " . tag($id, getName($id)['result']) . ".");
				sm($config['console'], "#UnBan #id$id \n<b>Unbanned user:</> " . tag($id, getName($id)['result']) . " [" . code($id) . "] \n<b>Date:</> " . date("d/m/Y h:i:s") . "\n<b>Unban by:</> " . tag());
			}
			die;
		}
		# JSON of User on the database
		if ($cmd == "database") {
			if ($typechat == "supergroup") {
				sm($chatID, code(json_encode($g, JSON_PRETTY_PRINT)));
			} else {
				sm($chatID, code(json_encode($u, JSON_PRETTY_PRINT)));
			}
			die;
		}
		# Manual queries
		if (strpos($cmd, "query ") === 0 and !$is_clone and $config['devmode']) {
			$query = str_replace('query ', '', $cmd);
			$q = $PDO->prepare($query);
			$q->execute();
			$r = $q->fetchAll();
			$error = $PDO->errorInfo();
			file_put_contents("query-r.json", json_encode($r, JSON_PRETTY_PRINT));
			sm($chatID, bold("Query: ") . code($query) . "\n" . bold("Result: ") . code(substr(json_encode($r), 0, 255)) . "\n" . bold("Errors: ") .	code(substr(json_encode($error), 0, 255)) );
			die;
		}
	}
	
	# Ban from the Bot
	if (!$isadmin) {
		if ($chatID > 0) {
			if (strpos($u['status'][$botID], "ban") === 0) {
				if ($u['status'][$botID] == "ban") {
					$json = json_decode($redis->get($userID), true);
					if ($json['ban'] !== true) {
						$json['ban'] = true;
						$redis->set($userID, json_encode($json));
					}
					// Ban a durata non specificata
				} else {
					$time = str_replace("ban", '', $u['status'][$botID]);
					if ($time < time()) {
						db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode([$botID => 'attivo']), $userID], "no");
						if ($config['usa_redis'] and isset($redis)) {
							rdel($chatID);
						}
						sm($chatID, "Sbannato dal Bot");
						sm($config['console'], "Utente sbannato: " . bold("$nome $cognome") . " [" . code($userID) . "] \nData corrente: " . date("c") . "\nData unban previsto: " . date("c", $time));
					}
				}
				die; // Ban per un utente
			}
		} else {
			if (strpos($g['status'][$botID], "ban") === 0) {
				if ($g['status'][$botID] == "ban") {
					// Ban a durata non specificata
				} else {
					$time = str_replace("ban", '', $g['status'][$botID]);
					if ($time < time()) {
						db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode([$botID => 'attivo']), $chatID], "no");
						if ($config['usa_redis'] and isset($redis)) {
							rdel($chatID);
						}
						sm($chatID, "Gruppo sbannato dal Bot");
						sm($config['console'], "Gruppo sbannato: " . bold("$title") . " [" . code($chatID) . "]");
					}
				}
				die; // Ban per un gruppo
			}
			if (strpos($c['status'][$botID], "ban") === 0) {
				if ($c['status'][$botID] == "ban") {
					// Ban a durata non specificata
				} else {
					$time = str_replace("ban", '', $g['status'][$botID]);
					if ($time < time()) {
						db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode([$botID => 'attivo']), $chatID], "no");
						if ($config['usa_redis'] and isset($redis)) {
							rdel($chatID);
						}
						sm($config['console'], "Canale sbannato: " . bold("$title") . " [" . code($chatID) . "]");
					}
				}
				die; // Ban per un canale
			}
		}
	}
} else {
	if ($cmd == "database" and $isadmin) {
		sm($chatID, "The database is off, activates a postgresql database to execute this Bot.");
	}
	die;
}
