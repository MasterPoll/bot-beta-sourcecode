<?php

# Performance del Bot
$times['finish'] = microtime(true);
if ($cmd == "mp_performance") {
	foreach($times as $type => $sectime) {
		if ($type !== 'start') {
			$nonsoperche = $sectime - $extime;
			$te[$type] = $nonsoperche;
			$extime = $sectime;
		} else {
			$extime = $sectime;
		}
	}
	$tot = number_format(microtime(true) - $times['start'], 6, ".", '');
	sm($chatID, bold("Performance del Bot 📶 ") . code("\nStart:      " . number_format($te['telegram_update'], 6, '.', '') . " secondi\nUpdate:     " . number_format($te['functions'], 6, '.', '') . " secondi\nFunzioni:   " . number_format($te['redis'], 6, '.', '') . " secondi\nRedis:      " . number_format($te['postgresql'], 6, '.', '') . " secondi\nPostgreSQL: " . number_format($te['plugins'], 6, '.', '') . " secondi\nPlugins:    " . number_format($te['finish'], 6, '.', '') . " secondi\nTotale:     $tot secondi"));
	die;
}

if ($cmd == "mp_ping") {
	$start_time = microtime(true);
	sm($chatID, "Pong!");
	$end_time = microtime(true);
	sm($chatID, "Request solved in " . round($end_time - $start_time, 6) . " seconds.");
	die;
}

# Fine dei plugins
if ($messageType == "callback_query") {
	cb_reply($cbid, '', false);
} elseif ($messageType == "text message" and $chatID) {
	sm($chatID, getTranslate('wannaCreate'));
} elseif ($messageType == "command") {
	sm($chatID, getTranslate('unrecognizedCommand'));
} else {
	if ($chatID) sm($chatID, getTranslate('wannaCreate'));
}
die;
