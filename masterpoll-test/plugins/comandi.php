<?php

if ($cbdata == "fatto") {
	$config['json_payload'] = true;
	dm($chatID, $cbmid);
    cb_reply($cbid, '👍', false);
	$api = $config['logger'];
	sm($config['logger_chat'], "#menu$cbmid #id$userID \n<b>Utente:</b> " . tag() . " [" . code($userID) . "]\n<b>Bot</>: #bot" . $botID);
    die;
}

if ($chatID == $config['sponsor']) {
	// Comandi del canale sponsor
	die;
} elseif ($chatID == $config['console']) {
	// Comandi del canale log
	if ($isadmin and $cbdata) {
		if (strpos($cbdata, "fix_") === 0) {
			cb_reply($cbid, '', false);
			$e = explode("-", str_replace('fix_', '', $cbdata));
			sm($e[0], getTranslate('bugFixed', false, getLanguage($e[0])), false, 'def', $e[1]);
			editMsg($chatID, "#BugFixed #id" . $e[0] . " \nBug fixato! ✅", $cbmid);
		}
		if (strpos($cbdata, "unknown_") === 0) {
			cb_reply($cbid, '', false);
			$e = explode("-", str_replace('unknown_', '', $cbdata));
			sm($e[0], getTranslate('unknownBug', false, getLanguage($e[0])), false, 'def', $e[1]);
			editMsg($config['console'], "#UnknownBugReport #id" . $e[0] . "\nSegnalazione non valida.", $cbmid);
		}
		if (strpos($cbdata, "notfix_") === 0) {
			cb_reply($cbid, '', false);
			$e = explode("-", str_replace('notfix_', '', $cbdata));
			sm($e[0], getTranslate('notBug', false, getLanguage($e[0])), false, 'def', $e[1]);
			editMsg($config['console'], "#BugReport #id" . $e[0] . "\n" . getTranslate('notBug'), $cbmid);
		}
	}
	die;
} elseif ($chatID < 0) {
	// Comandi in gruppi e canali
	if ($typechat == "group") {
		// Gruppi privati non supportati
		sm($chatID, "<b>Error #00:</b> type of chat not supported.");
		lc($chatID);
		die;
	}
	$config['json_payload'] = false;
	$config['response'] = false;
	if (strpos($g['status'][$botID], "premium") === 0 or strpos($c['status'][$botID], "premium") === 0) {
		// Supergruppi e canali Premium
		if (in_array($cmd, ['jsonpayload_flood928474'])) {
			$config['json_payload'] = true;
			sm($chatID, "Sas");
		}
		if (in_array($cmd, ['riavvia', 'restart', 'reload', 'reload_admins', 'rmp'])) {
			if ($redis->get("getchat-$chatID")) {
				die;
			} else {
				$redis->set("getchat-$chatID", true);
			}
			if ($g) $adminis = $g['admins'];
			if ($c) $adminis = $c['admins'];
			$adminis = json_decode($adminis, true);
			if (is_array($adminis)) {
				foreach($adminis as $ad) {
					if (round($ad['user']['id']) === round($userID)) $admin = true;
				}
			}
			if (!$admin and $adminis) die;
			if ($typechat == "supergroup") {
				$getchat = getChat($chatID);
				if (isset($getchat['result']['permissions'])) {
					$perms = $getchat['result']['permissions'];
				} else {
					$perms = ["can_send_messages" => true, "can_send_media_messages" => true, "can_send_polls" => true, "can_send_other_messages" => true, "can_add_web_page_previews" => true, "can_change_info" => false, "can_invite_users" => false, "can_pin_messages" => false];
				}
				$g['permissions'] = json_encode($perms);
				if (isset($getchat['result']['description'])) {
					$descrizione = $getchat['result']['description'];				
				} else {
					$descrizione = "";
				}
				$admins = getAdmins($chatID);
				if (isset($admins['ok'])) {
					$adminsg = json_encode($admins['result']);
				} else {
					$adminsg = "[]";
				}
				$q1 = db_query("UPDATE gruppi SET title = ?, username = ?, admins = ?, description = ?, permissions = ?, page = ? WHERE chat_id = ?", [$title, $usernamechat, $adminsg, $descrizione, $g['permissions'], " ", $chatID], "no");
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				$m = sm($chatID, "✅");
				sleep(1);
				$redis->del("getchat-$chatID");
				dm($chatID, $m['result']['message_id']);
			} else {
				db_query("UPDATE canali SET admins = ? WHERE chat_id = ?", [json_encode($adminis), $chatID], "no");
			}
			die;
		} elseif ($msg == "mp-check") {
			if ($c) {
				sm($chatID, "Premium Chat");
			}
			die;
		}
	} else {
		// Veridica del premium
		if ($g) $adminis = $g['admins'];
		if ($c) $adminis = $c['admins'];
		$adminis = json_decode($adminis, true);
		if (!$adminis) {
			$r = getAdmins($chatID);
			if ($r['ok']) {
				$adminis = $r['result'];
			} else {
				$adminis = [];
			}
			db_query("UPDATE gruppi SET admins = ? WHERE chat_id = ?", [json_encode($adminis), $chatID], "no");
			db_query("UPDATE canali SET admins = ? WHERE chat_id = ?", [json_encode($adminis), $chatID], "no");
		}
		if (!$adminis) {
			sm($chatID, "<b>Error:</b> administrators list not loaded.");
			lc($chatID);
			die;
		} else {
			foreach($adminis as $ad) {
				if ($ad['status'] == "creator") {
					$founder = $ad['user']['id'];
				}
			}
			if (!$founder) {
				sm($chatID, "<b>Error:</b> creator not found.");
				die;
			}
			$isp = isPremium($founder);
		}
		if ($isp) {
			$config['console'] = true;
			if ($typechat == "supergroup")  {
				$m = sm($chatID, bold("✅ Group Added!"));
				$g['status'][$botID] = "premium$founder";
				$q1 = db_query("UPDATE gruppi SET status = ? WHERE chat_id = ?", [json_encode($g['status']), $chatID]);
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sleep(5);
				dm($chatID, $m['result']['message_id']);
			} elseif ($typechat == "channel") {
				$m = sm($chatID, "✅ Test by @" . $config['username_bot']);
				$c['status'][$botID] = "premium$founder";
				$q1 = db_query("UPDATE canali SET status = ? WHERE chat_id = ?", [json_encode($c['status']), $chatID]);
				if (!$q1['ok']) {
					sm($chatID, "❌ " . getTranslate('generalError'));
					die;
				}
				sleep(1);
				dm($chatID, $m['result']['message_id']);
			} else {
				die;
			}
		} else {
			sm($chatID, "<b>Error:</b> The founder is not premium. [" . code($founder) . "]");
			lc($chatID);
		}
	}
	if (!$cbid) die;
}

$update = false;
if (!$classwork) require($f['class']);
if (!$classwork) {
	call_error("<b>Fatal Error:</b> le funzioni dei sondaggi non funzionano!");
	$text = '🤖 Bot Error: report it with /bug';
	if ($cbid) {
		cb_reply($cbid, $text, true);
	} else {
		sm($chatID, $text);
	}
	die;
}
date_default_timezone_set($u['settings']['timezone']);

if (in_array($cmd, ["beta", "changelog", "updates"])) {
	fw($chatID, "@MasterPollBeta", 4);
	die;
}

if ($cmd == "translator" or strpos($cbdata, "translations") === 0) {
	$traduttore = false;
	if ($cmd == "translator") {
		$cbdata = "translations_";
		$config['response'] = true;
		$m = sm($chatID, getTranslate('loading'));
		$cbmid = $m['result']['message_id'];
	}
	$q = db_query("SELECT choices FROM polls WHERE user_id = ? and poll_id = ?", [244432022, 97], true);
	if ($q['ok']) {
		$q = $q['result']['choices'];
	} else {
		if (!$cmd) cb_reply($cbid, '', false);
		editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
		die;
	}
	$whitelist = json_decode($q, true)['participants'];
	if (in_array($userID, $whitelist)) {
		$langsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$t = "🌐 " . bold(getTranslate('translatorModeButton')) . "\n";
		if (strpos($cbdata, "translations_") === 0) {
			if ($u['settings']['translator']) {
				$t .= getTranslate('translatorModeOff');
				$temoji = "▶️";
				$u['settings']['translator'] = false;
			} else {
				$t .= getTranslate('translatorModeOn');
				$temoji = "⏸";
				$u['settings']['translator'] = true;
			}
			db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		} else {
			if ($u['settings']['translator']) {
				$t .= getTranslate('translatorModeOn');
				$temoji = "⏸";
			} else {
				$t .= getTranslate('translatorModeOff');
				$temoji = "▶️";
			}
		}
		$menu[] = [
			[
				"text" => getTranslate('translatorModeButton') . " $temoji",
				"callback_data" => "translations_"
			]
		];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "settings"
			]
		];
		if (isset($linguebot[$lang])) {
			$perc = round(count(array_keys($linguebot[$lang])) / count(array_keys($linguebot['en'])) * 100, 2);
		} else {
			$perc = "0";
		}
		$t .= "\n" . bold(maiuscolo($langsname[$lang]) . ":") . " $perc%";
	} else {
		$t = "🚷 Access denied";
		$notranslator = true;
	}
	if ($notranslator) {
		cb_reply($cbid, $t, false);
		$cbdata = "settings";
	} else {
		cb_reply($cbid, '', false, $cbmid, $t, $menu);
		die;
	}
}

if ($cmd == "reset_advertising" and $config['devmode']) {
	unset($u['settings']['advertising']);
	unset($u['settings']['1hour']);
	$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	sm($chatID, getTranslate('done'));
	die;
}

if ($cmd == "recent_advertising" and $config['devmode']) {
	if ($u['settings']['advertising']) {
		foreach ($u['settings']['advertising'] as $id => $dtime) {
			$list .= "\n/advg_$id - " . date($u['settings']['date_format'], ($dtime - (60 * 60 * 24)));
		}
	} else {
		$list = "Nothing to show here...";
	}
	sm($chatID, bold("🗃 Advertising") . "\n$list");
	die;
}

if (strpos($cmd, "advg_") === 0) {
	$id = explode("_", $cmd, 2)[1];
	if (is_numeric($id) and $id) {
		$m = fw($userID, $config['sponsor'], $id);
	} else {
		sm($chatID, "Advertising not found...");
		die;
	}
	if ($m['ok']) {
	} else {
		sm($chatID, "Error " . $m['error_code'] . " returned. \n" . code($m['description']));
	}
	die;
}

if (strpos($cmd, "sponsor ") === 0 and $isadmin) {
	$config['response'] = true;
	$ex = explode(" ", $cmd, 3);
	if (!is_numeric($ex[1])) {
		username(str_replace("@", '', $ex[1]));
	}
	$id = str_replace("@", '', $ex[1]);
	if (isset($id)) {
		$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
		if ($q['ok']) {
			$q = $q['result'];
		} else {
			if (!$cmd) cb_reply($cbid, '', false);
			editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
			die;
		}
		$id = $q['user_id'];
		if (!$q['user_id']) {
			sm($chatID, "User not found in the database...");
			die;
		} elseif ($q['user_id'] == $userID) {
			sm($chatID, "You can't sas your self...");
			die;
		} else {
			$id = $q['user_id'];
		}
		$m = sm($q['user_id'], getTranslate('sponsorPayamentDone', false, $q['lang']));
		if ($m['ok']) {
			sm($chatID, "I have send the contact to " . tag($id, getName($id)['result']));
		} else {
			sm($chatID, "Error " . $m['error_code'] . " returned. \n" . code($m['description']));
		}
	}
	die;
}

if ($cmd == "pgift" and $isadmin) {
	if (!file_exists("/home/masterpoll-documents/premium-gift-keys.json")) {
		$json = [];
	} else {
		$json = json_decode(file_get_contents("/home/masterpoll-documents/premium-gift-keys.json"), true);
	}
	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
	$key = substr(str_shuffle($chars), 0, 8);
	$json[] = $key;
	file_put_contents("/home/masterpoll-documents/premium-gift-keys.json", json_encode($json, JSON_PRETTY_PRINT));
	sm($chatID, "Link: https://t.me/" . $config['username_bot'] . "?start=" . bot_encode("premium_$key"));
	sm($config['console'], "#Gift #id$userID \n" . tag() . " ha generato una chiave regalo Premium. \n<b>User:</b> " . tag() . " [" . code($userID) . "] \n<b>Chiave:</b> #$key");
	die;
}

if ($cmd == "advertising") {
	$config['console'] = false;
	$r = ssponsor($chatID, $lang, $u['settings']);
	if ($r['ok']) {
		$emoji = "✅";
		$message = $r['message'];
	} else {
		$emoji = "❌";
		$message = $r['description'];
	}
	if ($config['devmode']) {
		$dev = "\n" . $message;
	} else {
		$dev = "💚";
	}
	sm($chatID, " $emoji" . getTranslate('done') . $dev);
	die;
}

if ($cmd == 'login' and $config['devmode']) {
	if ($cognome) $nome .= " $cognome";
	sm($chatID, "Log in as $nome in our Website...", $menu);
	die;
}

if ($cmd == "web" and $config['devmode']) {
	if ($u['web_devices']) {
		$t = "<b>💻 Devices</b>";
		foreach ($u['web_devices'] as $device_name => $dinfo) {
			$t .= "\n- $device_name \nLast seen: " . date($u['settings']['date_format'], $dinfo['last_seen']);
		}
	} else {
		$t = "No device logged in with this account...";
	}
	$menu[] = [
		[
			"text" => "Log in as " . $nome,
			"login_url" => ["url" => "https://web.masterpoll.xyz/?bot=" . $config['username_bot'], "bot_username" => $config['username_bot'], "forward_text" => "Log in our Web Site for vote, manage and create polls.", "request_write_access" => true]
		]
	];
	sm($chatID, $t, $menu);
	die;
}

if (strpos($cmd, "website_") === 0 and $isadmin) {
	require("/home/masterpoll-documents/site-config.php");
	$config['encrypt_method'] = $config['site']['encrypt_method'];
	$config['secret_key'] = $config['site']['secret_key'];
	$config['secret_iv'] = $config['site']['secret_iv'];
	$e = bot_encode(str_replace("website_", '', $cmd));
	sm($chatID, "https://masterpoll.xyz/thread.php?p=" . $e);
	die;
}

if ($cmd == "time") {
	sm($chatID, "🕰 " . date($u['settings']['date_format']));
	die;
}

if (strpos($cbdata, 'dateformat') === 0) {
	$datetypes = ['D, j M Y H:i', 'd/m/Y H:i', 'd/Y/m H:i', 'm/d/Y H:i', 'm/Y/d H:i', 'Y/d/m H:i', 'Y/m/d H:i'];
	$t = bold("🔢 " . getTranslate('dateFormat'));
	if (strpos($cbdata, 'dateformat_') === 0) {
		$u['settings']['date_format'] = str_replace('dateformat_', '', $cbdata);
		$q = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		if (!$q['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
	}
	foreach($datetypes as $datetype) {
		if ($datetype == $u['settings']['date_format']) {
			$emo = "✅";
		} else {
			$emo = "☑️";
		}
		$menu[] = [
			[
				"text" => "$emo " . date($datetype),
				"callback_data" => "dateformat_" . $datetype
			]
		];
	}
	$oktype = $u['settings']['date_format'];
	$tov = [
		"Y" => "YYYY",
		"D" => "Day",
		"l" => "Today",
		"j" => "D",
		"d" => "DD",
		"M" => "Mon",
		"m" => "MM",
		"H" => "HH",
		"i" => "Mi"
	];
	foreach($tov as $k => $v) {
		$oktype = str_replace($k, $v, $oktype);
	}
	$t .= "\n" . $oktype;
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "changeTimeZone"
		]
	];
	cb_reply($cbid, '', false, $cbmid, $t, $menu);
	die;
}

if ($cbdata == "help") {
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('mainMenuButton'),
			"callback_data" => "startMessage"
		]
	];
	if ($u['settings']['premium']) {
		$linkvip = $config['links']['premiumGroupInvite'];
	} else {
		$linkvip = $config['links']['premiumFAQ'];
	}
	$viproom = "\n⭐️ " . text_link("VIP Room", $linkvip);
	$t = bold("ℹ️ " . getTranslate('helpButton')) . "\n\n❓ " . text_link(getTranslate('readFAQ'), $config['links']['FAQ']) . "\n⚠️ " . getTranslate('reportAnError') . "\n\n👥 " . text_link("MasterPoll | Chat", $config['links']['group']) . "$viproom\n💬 " . getTranslate('contactUsSupport');
	cb_reply($cbid, '', false, $cbmid, $t, $menu);
	die;
}

if ($cbdata == "viproom") {
	if ($u['settings']['premium']) {
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "settings"
			]
		];
		if ($u['settings']['premium'] == "lifetime") {
			$timep = "Life Time";
		} else {
			$timep = date($u['settings']['date_format'], $u['settings']['premium']);
		}
		cb_reply($cbid, getTranslate('youArePremium'), false, $cbmid, getTranslate('premiumInfo', [$timep, $config['links']['premiumGroupInvite']]), $menu);
		die;
	} else {
		$cbdata = "shop";
		$novip = true;
		cb_reply($cbid, getTranslate('noVipPerms'), false);
	}
}

if ($cbdata == "revoke-api") {
	$char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	$chiave = substr(str_shuffle($char), 0, 35);
	$r = rand(10, 35);
	if ($r < 20) {
		$chiave[$r] = "-";
	} else {
		$chiave[$r] = "_";
	}
	$u['settings']['token'] = $chiave;
	$q = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
	if (!$q['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		]
	];
	$token = $userID . ":" . $u['settings']['token'];
	$t = getTranslate('apiAccessTokenRevoked', [getName($userID)['result'], $token]);
	cb_reply($cbid, '', false, $cbmid, $t, $menu);
	die;
}

if ($cmd == "api" or $cbdata == "api") {
	if (!isset($u['settings']['token'])) {
		$char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
		$chiave = substr(str_shuffle($char), 0, 35);
		$r = rand(10, 35);
		if ($r < 20) {
			$chiave[$r] = "-";
		} else {
			$chiave[$r] = "_";
		}
		$u['settings']['token'] = $chiave;
		$q = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		if (!$q['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		} else {
			sm($config['console'], "#newapi #id$userID\n<b>Utente:</> " . tag());
		}
	}
	$menu[] = [
		[
			"text" => getTranslate('revoke'),
			"callback_data" => "revoke-api"
		]
	];
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		]
	];
	$token = $userID . ":" . $u['settings']['token'];
	$t = getTranslate('apiAccessToken', [$token]);
	if ($cbdata and $cbid) {
		cb_reply($cbid, '', false, $cbmid, $t, $menu);
	} else {
		sm($chatID, $t, $menu);
	}
	die;
}

if ($u['settings']['type'] == "participation") {
	$u['settings']['anonymous'] = false;
}

if ($posizione) {
	$timezone = get_nearest_timezone($rupdate["message"]["location"]["latitude"], $rupdate["message"]["location"]["longitude"]);
	date_default_timezone_set($timezone);
	$u['settings']['timezone'] = $timezone;
	$q = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], 'no');
	if (!$q['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	sm($chatID, getTranslate('timezoneSet', [$timezone, date("P")]), 'nascondi');
	$cmd = "start";
}

if (strpos($cbdata, "anonymous-") === 0) {
	$anonymous = json_decode(str_replace("anonymous-", '', $cbdata));
	$u['settings']['anonymous'] = $anonymous;
	if ($u['settings']['type'] == "participation" and $anonymous == "true") {
		$u['settings']['anonymous'] = false;
		cb_reply($cbid, '⛔️', false);
		die;
	}
	if (!isset($u['settings']['type'])) {
		$u['settings']['type'] = "voto";
	}
	$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$cbdata = "createNewPoll";
}

if (strpos($cbdata, "changetype-") === 0) {
	$type = str_replace("changetype-", '', $cbdata);
	if ($config['types'][$type]) {
		$u['settings']['type'] = $type;
		if ($u['settings']['type'] == "participation") {
			$emo = "✅";
			$emoa = "☑️";
			$u['settings']['anonymous'] = false;
		} elseif ($u['settings']['anonymous']) {
			$emoa = "✅";
			$emo = "☑️";
		} else {
			$emo = "✅";
			$emoa = "☑️";
		}
		$q = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		if (!$q['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
		$cbdata = "createNewPoll";
	} else {
		$cbdata = "changetype";
	}
}

if ($cbdata == "changetype") {
	$r = getTranslate('pollTypeSelection');
	$secnum = 1;
	foreach ($config['types'] as $type => $act) {
		if ($act) {
			$num += 1;
			$te = maiuscolo($type);
			if ($type == $u['settings']['type']) {
				$emo = "✅";
			} else {
				$emo = "☑";
			}
			if (!$finish) {
				if ($secnum !== $num) {
					$secnum = $num + 1;
					if (isset($ex2)) {
						$menu[] = [
							[
								"text" => $extype,
								"callback_data" => $ex2
							],
							[
								"text" => getTranslate("inlineButton$te", [$emo]),
								"callback_data" => "changetype-$type"
							]
						];
						unset($ex2);
					} else {
						$menu[] = [
							[
								"text" => getTranslate("inlineButton$te", [$emo]),
								"callback_data" => "changetype-$type"
							]
						];
						$finish = true;
					}
				} else {
					$extype = getTranslate("inlineButton$te", [$emo]);
					$ex2 = "changetype-$type";
				}
			}
			$type = maiuscolo($type);
			$type[0] = strtolower($type[0]);
			$r .= "\n" . bold(maiuscolo(getTranslate($type))) . " - " . getTranslate('pollType' . $te . 'Description') . "\n";
		} else {
			$type = maiuscolo($type);
			$type[0] = strtolower($type[0]);
			if ($config['devmode']) $r .= "\n⛔️ " . bold(maiuscolo(getTranslate($type))) . "\n";
		}
	}
	if (isset($ex2)) {
		$menu[] = [
			[
				"text" => $extype,
				"callback_data" => $ex2
			]
		];
		unset($ex2);
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "createNewPoll"
		]
	];
	cb_reply($cbid, '', false, $cbmid, $r, $menu);
	die;
}

if (strpos($cbdata, "cancel_poll") === 0) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], 'no');
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	$cbdata = "update_" . str_replace('cancel_poll', '', $cbdata);
}

if ($cbdata == "cancel_createNewPoll") {
	$config['json_payload'] = false;
	if (isset($u['settings']['createpoll'])) {
		unset($u['settings']['createpoll']);
		$cancelled = getTranslate('hasCanceled');
	} else {
		$cancelled = getTranslate('canceledThat');
	}
	$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ["", json_encode($u['settings']), $userID], 'no');
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	cb_reply($cbid, $cancelled, false);
	$cbdata = "startMessage";
}

if ($cmd == "start noautopin") {
	sm($chatID, italic("✅ Thanks for using the bot from @NoAutoPinBot!"));
} elseif ($cmd == "start botostore") {
	sm($chatID, italic("✅ Thanks for adding the bot from botostore.com!"));
} elseif ($cmd == "start start_tgstat") {
	sm($chatID, italic("✅ Thanks for adding the bot from TGStat.com!"));
}
if (in_array($cmd, ["start", "start inlines", "start botostore", "start start_tgstat", "start noautopin"]) or $cbdata == "startMessage") {
	if ($u['status'][$botID] !== "avviato") {
		$u['status'][$botID] = "avviato";
		db_query("UPDATE utenti SET status = ? WHERE user_id = ?", [json_encode($u['status']), $userID], "no");
		$langsname = json_decode('{"en":"🇬🇧 English","de":"🇩🇪 Deutsch","it":"🇮🇹 Italiano","pt":"🇧🇷 Português","he":"🇮🇱 עברית","uk":"🇺🇦 Українська","nb_NO":"🇳🇴 Norsk","fa":"🇮🇷 پارسی","es":"🇪🇸 Español","zh_TW":"🇹🇼 台灣正體","zh_HK":"🇭🇰 港澳正體","ja":"🇨🇳 中囯簡体","ru":"🇷🇺 Русский","fr":"🇫🇷 Français","uz":"🇺🇿 O\'zbek"}', true);
		$langsname[$lang] = $langsname[$lang] . " ✅";
		foreach ($langsname as $lingua => $nl) {
			if (!isset($num)) {
				$num = 0;
			} else {
				$num = $numSecur + 1;
			}
			$numSecur = $num;
			if (!$finish) {
				if ($num == 0) {
					$secnum = 1;
				}
				if ($secnum !== $num) {
					$secnum = $num + 1;
					if (isset($nl2)) {
						$menu[] = array(
							array(
								"text" => $nl,
								"callback_data" => "/setlang-$lingua"
							),
							array(
								"text" => $nl2,
								"callback_data" => "/setlang-$lingua2"
							),
						);
					} else {
						//$finish = true;
						$menu[] = array(
							array(
								"text" => $nl,
								"callback_data" => "/setlang-$lingua"
							),
						);
					}
				} else {
					$lingua2 = $lingua;
					$nl2 = $nl;
				}
			}
		}
		$menu[] = [
			[
				"text" => "💾 " . getTranslate('done'),
				"callback_data" => "settings"
			]
		];
		sm($chatID, getTranslate('setLanguage'), $menu);
		die;
	}
	if ($u['settings']['anonymous']) {
		$emoa = "✅";
		$emo = "☑️";
	} else {
		$emo = "✅";
		$emoa = "☑️";
	}
	$menu[] = [
		[
			"text" => "🆕 " . getTranslate('createNewPollButton') . " 🆕",
			"callback_data" => "createNewPoll"
		]
	];
	$menu[] = [
		[
			"text" => "⚙️ " . getTranslate('settingsButton'),
			"callback_data" => "settings"
		],
		[
			"text" => "🗃 " . getTranslate('pollListButton'),
			"callback_data" => "list"
		]
	];
	$menu[] = [
		[
			"text" => "ℹ️ " . getTranslate('helpButton'),
			"callback_data" => "help"
		],
		[
			"text" => "🛍 " . getTranslate('shopButton'),
			"callback_data" => "shop"
		]
	];
	$getlast = db_query("SELECT * FROM polls WHERE user_id = ? and status != ? ORDER BY poll_id DESC", [$userID, 'deleted'], true);
	if ($getlast['ok']) {
		if ($getlast['result']) {
			$p = doPoll($getlast['result']);
			if ($p['ok']) {
				$p = $p['result'];
				$type = maiuscolo($p['type']);
				$type[0] = strtolower($type[0]);
				$type = maiuscolo(getTranslate($type));
				if ($p['anonymous']) {
					$anonemo = "✅";
				} else {
					$anonemo = "☑️";
				}
				if (strlen($p['title']) < 32) {
					$title = bold($p['title']);
				} else {
					$title = bold(substr($p['title'], 0, 32)) . "...";
				}
				$lastpoll = "\n⏳ " . getTranslate('lastPollCreated') . ": /" . $p['poll_id'] . "\n  " . $title . "\n  " . getTranslate('pollType') . ": $type\n  " . getTranslate('pollAnonymous') . ": $anonemo\n";
			}
		}
	}
	$t = "🗃 " . str_replace("[0]", $lastpoll, getTranslate('startMessage')) . $cmessage;
	if ($cbdata == "startMessage") {
		cb_reply($cbid, '', false, $cbmid, $t, $menu);
	} else {
		sm($chatID, $t, $menu);
	}
	die;
}

if ($cbdata == "createNewPoll") {
	if ($u['page'] !== "newpoll") {
		$u['page'] = "newpoll";
		if (isset($u['settings']['createpoll'])) unset($u['settings']['createpoll']);
		$q1 = db_query("UPDATE utenti SET page = ?, settings = ? WHERE user_id = ?", ["newpoll", json_encode($u['settings']), $userID], 'no');
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
	}
	if ($u['settings']['anonymous']) {
		$emoa = "✅";
		$emo = "☑️";
	} else {
		$emo = "✅";
		$emoa = "☑️";
	}
	if (!isset($u['settings']['type'])) {
		$u['settings']['type'] = "vote";
		$q1 = db_query("UPDATE utenti SET settings = ? WHERE user_id = ?", [json_encode($u['settings']), $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
	}
	$type = maiuscolo($u['settings']['type']);
	$type[0] = strtolower($type[0]);
	$type = maiuscolo(getTranslate($type));
	$menu[] = [
		[
			"text" => "ℹ️ " . getTranslate('changePollTypeButton'),
			"callback_data" => "changetype"
		]
	];
	if (!in_array($u['settings']['type'], ['participation'])) {
		$menu[] = [
			[
				'text' => getTranslate('personalButton', [$emo]),
				'callback_data' => "anonymous-false"
			],
			[
				'text' => getTranslate('anonymousButton', [$emoa]),
				'callback_data' => "anonymous-true"
			]
		];
	}
	$menu[] = [
		[
			'text' => "🔙 " . getTranslate('mainMenuButton'),
			'callback_data' => "cancel_createNewPoll"
		]
	];
	cb_reply($cbid, '', false, $cbmid, "🆕 " . str_replace('[0]', bold("ℹ️ " . getTranslate('pollType')) . ": $type \n" . bold("👁‍🗨 " . getTranslate('pollAnonymous')) . ": $emoa", getTranslate('createNewPoll')), $menu);
	die;
}

if ($cbdata == "settings") {
	$r .= "\n🕰 " . bold(getTranslate('changeTimeZoneButton')) . " - " . getTranslate('settingsDescriptionChangeTimeZone');
	$menup1[] = [
		"text" => "🕰 " . getTranslate('changeTimeZoneButton'), 
		"callback_data" => "changeTimeZone"
	];
	$menup1[] = [
		"text" => "🔤 " . getTranslate('changeLanguageButton'),
		"callback_data" => "changeLanguage"
	];
	$r .= "\n🔤 " . bold(getTranslate('changeLanguageButton')) . " - " . getTranslate('settingsDescriptionChangeLanguage');
	$menup2[] = [
		"text" => "🚷 " . getTranslate('banlistButton'),
		"callback_data" => "banlist-view"
	];
	if ($u['theme'] == "light") {
		$temoji = "🌕";
	} else {
		$temoji = "🌑";
	}
	$r .= "\n" . bold(getTranslate('changeTheme', [$temoji])) . " - " . getTranslate('changeThemeDescription');
	$menup2[] = [
		"text" => getTranslate('changeTheme', [$temoji]),
		"callback_data" => "changeTheme"
	];
	$r .= "\n🚷 " . bold(getTranslate('banlistButton')) . " - " . getTranslate('settingsDescriptionBannedUsers');
	if (isPremium($userID, $u)) {
		$menup2[] = [
			"text" => "⭐️ " . getTranslate('vipRoomButton'),
			"callback_data" => "viproom"
		];
		$r .= "\n⭐️ " . bold(getTranslate('vipRoomButton')) . " - " . getTranslate('settingsDescriptionVipRoom', [$config['links']['premiumFAQ']]);
	}
	$q = db_query("SELECT choices FROM polls WHERE user_id = ? and poll_id = ?", [244432022, 97], true);
	if ($q['ok']) {
		$q = $q['result']['choices'];
	} else {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
		die;
	}
	if ($q) {
		$whitelist = json_decode($q, true)['participants'];
		if (in_array($userID, $whitelist)) {
			$menup4[] = [
				"text" => "🌐 " . getTranslate('translationsButton'),
				"callback_data" => "translations"
			];
			$r .= "\n🌐 " . bold(getTranslate('translationsButton')) . " - " . getTranslate('settingsDescriptionTranslations');
		}
	}
	if (isset($u['settings']['token'])) {
		$menup4[] = [
			"text" => "🤖 API Token",
			"callback_data" => "api"
		];
		$r .= "\n🤖 " . bold("API Token") . " - " . getTranslate('settingsDescriptionAPI');
	}
	if (isset($menup1)) $menu[] = $menup1;
	if (isset($menup2)) $menu[] = $menup2;
	if (isset($menup3)) $menu[] = $menup3;
	if (isset($menup4)) $menu[] = $menup4;
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('mainMenuButton'),
			"callback_data" => "startMessage"
		]
	];
	$t = bold(getTranslate('settings')) . "\n$r";
	cb_reply($cbid, '', false, $cbmid, $t, $menu);
	die;
}

if (strpos($cbdata, "changeTheme") === 0) {
	if (strpos($cbdata, "changeTheme_") === 0) {
		$u['theme'] = str_replace('changeTheme_', '', $cbdata);
		if (!in_array($u['theme'], ['light', 'dark'])) die;
		db_query("UPDATE utenti SET theme = ? WHERE user_id = ?", [$u['theme'], $userID], 'no');
	}
	$emoji['dark'] = "🌑";
	$emoji['light'] = "🌕";
	$emoji['sossio'] = "🔪";
	$emoji[$u['theme']] .= " ✅";
	$t = "<a href='https://web.masterpoll.xyz/assets/bg_{$u['theme']}.png'>&#8203;</>" . bold(getTranslate('changeTheme', ["🌗"])) . "\n" . getTranslate('changeThemeDescription');
	$menu[] = [
		[
			"text" => $emoji['light'],
			"callback_data" => "changeTheme_light"
		],
		[
			"text" => $emoji['dark'],
			"callback_data" => "changeTheme_dark"
		]
	];
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		]
	];
	cb_reply($cbid, '', false, $cbmid, $t, $menu, 'def', false);
	die;
}

if (strpos($cbdata, "default_settings") === 0) {
	if (strpos($cbdata, "default_settings_") === 0) {
		$action = str_replace("default_settings_", '', $cbdata);
		if ($action === "false") {
			unset($u['settings']['default_settings']);
		} elseif ($action === "true") {
			$u['settings']['default_settings'] = [];
		}
	}
	if (isset($u['settings']['default_settings'])) {
		$acvtive = true;
	} else {
		$acvtive = false;
	}
	if ($active) {
		$menu[] = [
			[
				"text" => "✅ " . getTranslate('deactivateButton'),
				"callback_data" => "default_settings_false"
			]
		];
	} else {
		$menu[] = [
			[
				"text" => "☑️ " . getTranslate('activateButton'),
				"callback_data" => "default_settings_true"
			]
		];
	}
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		]
	];
	cb_reply($cbid, '', false, $cbmid, $t, $menu);
	die;
}

if (strpos($cbdata, "banlist-") === 0) {
	$e = explode("-", $cbdata);
	if ($e[1] == "view") {
		if ($u['banlist']) {
			if (count($u['banlist']) >= 50) {
				$lgif = loading_gif();
				cb_reply($cbid, '', false, $cbmid, "<a href='$lgif'>&#8203</>" . italic(getTranslate('loading')), false, 'def', false);
				$last_time = time();
			}
			foreach($u['banlist'] as $user => $ban) {
				if (!is_numeric($ban)) {
					$t .= "\n" . tag($user, getName($user)['result']) . ": ⏱♾";
					$bans[$user] = true;
				} else {
					if ($ban <= time()) {
						
					} else {
						$dateban = date($u['settings']['date_format'], $ban);
						$t .= "\n" . tag($user, getName($user)['result']) . ": ⏱$dateban";
						$bans[$user] = $ban;
					}
				}
			}
			$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($bans), $userID], "no");
			if (!$q1['ok']) {
				sm($chatID, "❌ " . getTranslate('generalError'));
				die;
			} else {
				$u['banlist'] = $bans;
			}
			if (!$t) {
				$t = getTranslate('listEmpty');
			}
		} else {
			$t = getTranslate('listEmpty');
		}
		$menu[] = [
			[
				"text" => "🔄 " . getTranslate('commPageRefresh'),
				"callback_data" => $cbdata
			],
			[
				"text" => "🤖 Bottino Blocklist",
				"callback_data" => "banlist-bottino"
			]
		];
		$mmenu[] = [
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		];
		$mmenu[] = [
			"text" => "➕ " . getTranslate('addButton'),
			"callback_data" => "banlist-add"
		];
		if ($u['banlist']) {
			$mmenu[] = [
				"text" => "➖ " . getTranslate('removeButton'),
				"callback_data" => "banlist-remove"
			];
		}
		$menu[] = $mmenu;
		if (($last_time + 1) >= time()) sleep(1);
		cb_reply($cbid, '', false, $cbmid, "🚷 " . bold(getTranslate('banlistTitle')) . "\n$t", $menu);
	} elseif ($e[1] == "bottino") {
		if ($e[2]) {
			if ($e[2] == "true") {
				$u['bottino'] = true;
			} else {
				$u['bottino'] = false;
			}
			db_query("UPDATE utenti SET bottino = ? WHERE user_id = ?", [$e[2], $userID], 'no');
		}
		if ($u['bottino']) {
			$bottino = "✅";
			$no = "☑️";
		} else {
			$bottino = "☑️";
			$no = "✅";
		}
		$menu[] = [
			[
				"text" => "$bottino " . getTranslate('yes'),
				"callback_data" => "banlist-bottino-true"
			],
			[
				"text" => "$no " . getTranslate('no'),
				"callback_data" => "banlist-bottino-false"
			]
		];
		$menu[] = [
			[
				"text" => "🔙 " . getTranslate('backButton'),
				"callback_data" => "banlist-view"
			]
		];
		cb_reply($cbid, '', false, $cbmid, getTranslate('bottinoInfo', [$bottino]), $menu);
	} elseif ($e[1] == "add") {
		cb_reply($cbid, getTranslate('banlistAddOption'), true);
	} elseif ($e[1] == "remove") {
		cb_reply($cbid, getTranslate('banlistRemoveOption'), true);
	} else {
		cb_reply($cbid, '', false);
	}
	die;
}

if (strpos($cmd, "ban ") === 0 and $typechat == "private") {
	$ex = explode(" ", $cmd, 3);
	if (!is_numeric($ex[1])) {
		username(str_replace("@", '', $ex[1]));
	}
	$id = str_replace("@", '', $ex[1]);
	if (isset($id)) {
		$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
		if ($q['ok']) {
			$q = $q['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$id = $q['user_id'];
		if (isset($ex[2])) {
			$dat = $ex[2];
			if (strpos($dat, "/") !== false) {
				$exp = explode(" ", $dat);
				$data = explode("/", $exp[0]);
				if (isset($exp[1])) {
					$ora = explode(":", $exp[1]);
				} else {
					$ora = "000";
				}
				$dateban = mktime($ora[0], $ora[1], $ora[2], $data[1], $data[0], $data[2]);
				if ($dateban < time()) {
					$t = getTranslate('banlistAddInvalidTime');
					$t = str_replace("[0]", date("H:i:s", $dateban), $t);
					$t = str_replace("[1]", date("d/m/Y", $dateban), $t);
					sm($chatID, $t);
					die;
				}
			} elseif (strpos($dat, "for ") !== false) {
				$exp = strtolower(str_replace("for ", '', $dat));
				$dateban = strtotime($exp);
				if ($dateban <= time()) {
					$t = getTranslate('banlistAddInvalidTime');
					$t = str_replace("[0]", date("H:i:s", $dateban), $t);
					$t = str_replace("[1]", date("d/m/Y", $dateban), $t);
					sm($chatID, $t);
					die;
				}
			} else {
				$t = getTranslate('banlistAddInvalidTime');
				$t = str_replace("[0]", date("H:i:s", $dateban), $t);
				$t = str_replace("[1]", date("d/m/Y", $dateban), $t);
				sm($chatID, $t);
				die;
			}
			$al = date($u['settings']['date_format'], $dateban);
			$stato = $dateban;
		} else {
			$al = getTranslate('indefinitelyTime');
			$stato = true;
		}
		if (!$q['user_id']) {
			sm($chatID, getTranslate('userNotFound'));
			die;
		} elseif ($q['user_id'] == $userID) {
			sm($chatID, getTranslate('banlistAddMe'));
			die;
		} elseif (in_array($q['user_id'], array_keys($u['banlist'])) and !$ex[2]) {
			sm($chatID, getTranslate('banlistAlreadyAdded'));
			die;
		} else {
			$id = $q['user_id'];
		}
		if ($config['usa_redis'] and isset($redis)) {
			rdel("name$id");
		}
		$u['banlist'][$id] = $stato;
		$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($u['banlist']), $userID], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$t = getTranslate('banlistAdd');
		$t = str_replace("[0]", tag($id, getName($id)['result']), $t);
		$t = str_replace("[1]", $al, $t);
		sm($chatID, $t);
	}
	die;
}

if (strpos($cmd, "unban ") === 0 and $typechat == "private") {
	$ex = explode(" ", $cmd, 2);
	if (!is_numeric($ex[1])) {
		username(str_replace("@", '', $ex[1]));
	}
	$id = str_replace("@", '', $ex[1]);
	if (isset($id)) {
		$q = db_query("SELECT * FROM utenti WHERE user_id = ? or username = ?", [round($id), $id], true);
		if ($q['ok']) {
			$q = $q['result'];
		} else {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		if (!$q['user_id']) {
			sm($chatID, getTranslate('userNotFound'));
			die;
		} elseif (!in_array($q['user_id'], array_keys($u['banlist']))) {
			sm($chatID, getTranslate('banlistNotAdded'));
			die;
		} else {
			$id = $q['user_id'];
		}
		unset($u['banlist'][$id]);
		if ($config['usa_redis'] and isset($redis)) {
			rdel("name$id");
		}
		$q1 = db_query("UPDATE utenti SET banlist = ? WHERE user_id = ?", [json_encode($u['banlist']), round($userID)], "no");
		if (!$q1['ok']) {
			sm($chatID, "❌ " . getTranslate('generalError'));
			die;
		}
		$t = getTranslate('banlistRemove');
		$t = str_replace("[0]", tag($id, getName($id)['result']), $t);
		sm($chatID, $t);
	}
	die;
}

if ($cbdata == "setTimeZone") {
	dm($chatID, $cbmid);
	cb_reply($cbid, '', false);
	$menu[] = [
		[
			"text" => getTranslate('sendMyLocation'),
			"request_location" => true
		]
	];
	$menu[] = ["/cancel"];
	sm($chatID, getTranslate('timezoneSet', [$u['settings']['timezone'], date("P")]) . "\n\n" . getTranslate('changeTimeZone', [$u['settings']['timezone']]), $menu, 'def', false, false, false);
	die;
}

if ($cbdata == "changeTimeZone") {
	$menu[] = [
		[
			"text" => "📍 " . getTranslate('setMyTimeZone'),
			"callback_data" => "setTimeZone"
		],
		[
			"text" => "🔢 " . getTranslate('dateFormat'),
			"callback_data" => "dateformat"
		]
	];
	$menu[] = [
		[
			"text" => "🔙 " . getTranslate('backButton'),
			"callback_data" => "settings"
		]
	];
	cb_reply($cbid, '', false, $cbmid, getTranslate('timezoneSet', [$u['settings']['timezone'], date("P")]) . "\n\n" . getTranslate('changeTimeZone', [$u['settings']['timezone']]), $menu);
	die;
}
