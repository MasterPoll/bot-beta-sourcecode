<?php

$lang = $u['lang'];
if (!$exists_user or !$lang) {
	$lang = 'en';
}

function oneskyapp ($path = "/", $args = []) {
	$exargs = $args;
	$url = "https://platform.api.onesky.io/1$path";
	date_default_timezone_set("GMT");
	$time = time();
	$args = array_merge([
		'api_key' => "dUItn8VTaY3PxfUFMyPoNxJUHC2E9zmd",
		'timestamp' => $time,
		'dev_hash' => md5($time . "UGo0qCzBFccjPJmfhGOjMoSvhj2h7HGR")
	], $args);
	$o = sendRequest($url, $args, true, 'GET');
	$json = json_decode($o, true);
	if ($json) {
		$json['args'] = $exargs;
		return $json;
	}
	return $o;
}

function oneskyapp2 ($path = "/", $args = []) {
	$exargs = $args;
	$url = "http://api.oneskyapp.com/2$path";
	date_default_timezone_set("GMT");
	$time = time();
	$args = array_merge([
		'api-key' => "dUItn8VTaY3PxfUFMyPoNxJUHC2E9zmd",
		'timestamp' => $time,
		'dev-hash' => md5($time . "UGo0qCzBFccjPJmfhGOjMoSvhj2h7HGR")
	], $args);
	$o = sendRequest($url, $args, true, 'GET');
	$json = json_decode($o, true);
	if ($json) {
		$json['args'] = $exargs;
		return $json;
	}
	return $o;
}

function getLanguage($userID = false) {
	if (!$userID) return 'en';
	$q = db_query("SELECT lang FROM utenti WHERE user_id = ? LIMIT 1", [$userID], true);
	if ($q['ok']) {
		$q = $q['result'];
	} else {
		return 'en';
	}
	if (isset($q['lang'])) {
		return $q['lang'];
	} else {
		return 'en';
	}
}

if (!file_exists("/home/masterpoll-documents/" . $f['languages'])) {
	//file_put_contents("/home/masterpoll-documents/" . $f['languages'], "[]");
}
if (!isset($linguefile)) {
	$linguefile = file_get_contents("/home/masterpoll-documents/" . $f['languages']);
}
$linguebot = json_decode($linguefile, true);
if (!is_array($linguebot) or !$linguebot) {
	call_error("Il file delle lingue è corrotto! \n" . code(substr(json_encode(error_get_last()), 0, 256)));
	die;
} elseif ($config['devmode']) {
	if (rget('language-file-beta') !== true) {
		rset('language-file-beta', true);
		$exar = $linguebot;
		ksort($linguebot);
		foreach($linguebot as $tlang => $rr) {
			ksort($rr);
			$linguebot[$tlang] = $rr;
		}
		if ($exar !== $linguebot) file_put_contents("/home/masterpoll-documents/" . $f['languages'], json_encode($linguebot, JSON_PRETTY_PRINT));
		rdel('language-file-beta');
	}
}

if ($u['settings']['translator']) {
	$traduttore = true;
} else {
	$traduttore = false;
}

$dialetti = [
	'it' => [
		'it',	// Predefinito
		'sc', 	// Sardo
		'scn', 	// Siciliano
		'nap' 	// Napoletano
	]
];

function getTranslate($testo = 'start', $arr = [], $langp = 'def') {
	global $config;
	global $lang;
	global $linguebot;
	global $traduttore;
	global $dialetti;

	if ($traduttore) return $testo;
	if (!is_array($linguebot)) {
		call_error("Language Error: il file delle lingue non è stato caricato!");
		die;
	}
	if ($langp == 'def') {
		$langp = $lang;
	}
	$testo = str_replace(' ', '', $testo);
	if (in_array($lang, $dialetti['it'])) {
		if (isset($linguebot[$langp][$testo])) {
			$sas = $linguebot[$langp][$testo];
		} elseif (isset($linguebot['it'][$testo])) {
			$sas = $linguebot['it'][$testo];
		} elseif (isset($linguebot['en'][$testo])) {
			$sas = $linguebot['en'][$testo];
		} else {
			call_error("Language Warning: Impossibile trovare la stringa '$testo' sulla lingua $lang");
			$sas = "🤖";
		}
	} else {
		if (isset($linguebot[$langp][$testo])) {
			$sas = $linguebot[$langp][$testo];
		} elseif (isset($linguebot['en'][$testo])) {
			$sas = $linguebot['en'][$testo];
		} else {
			call_error("Language Warning: Impossibile trovare la stringa '$testo' sulla lingua $lang");
			$sas = "🤖";
		}
	}
	if (is_array($arr) and $arr) {
		$arr = array_values($arr);
		$e[0] = htmlspecialchars($arr[0]);
		$sas = str_replace("[0]", $e[0], $sas);
		$range = range(1, 10);
		foreach($range as $num) {
			if (isset($arr[$num])) {
				$e[$num] = htmlspecialchars($arr[$num]);
				$sas = str_replace("[$num]", $e[$num], $sas);
			}
		}
	}
	if ($traduttore) $trad = "$testo: ";
	return mb_convert_encoding($trad . $sas, "UTF-8");
}

function maiuscolo($text = "") {
	if (!$text) return "Null";
	if (strpos($text, " ") === false) {
		$text[0] = strtoupper($text[0]);
	} else {
		$spazi = explode(" ", $text);
		$text = "";
		foreach($spazi as $testo) {
			if ($testo !== $spazi[0]) $text .= " ";
			if (isset($testo[0])) {
				$testo[0] = strtoupper($testo[0]);
				$text .= $testo;
			}
		}
	}
	return $text;
}

if ($is_clone) {
	$bot = db_query("SELECT * FROM bots WHERE bot_id = ?", [$botID], true)['result'];
	if ($isadmin and $cmd == "dbot") {
		sm($chatID, code(json_encode($bot)));
		die;
	}
	if ($bot['bot_id'] == $botID) {
		$config['username_bot'] = $bot['username_bot'];
		if ($bot['owner_id'] == $userID) $is_owner = true;
		if (urldecode(bot_decode($bot['password'])) !== urldecode($_GET['password'])) {
			if ($is_owner or $isadmin) sm($chatID, "Bad Request: wrong webhook password");
			die;
		}
		$bot['master_status'] = json_decode($bot['master_status'], true);
		$bot['status'] = json_decode($bot['status'], true);
		if ($bot['master_status']) {
			if ($bot['status']) {
				
			} else {
				if ($typechat == "private") {
					$t = "🔴 " . getTranslate('disabledCloneByCreator');
					if ($cbid) {
						cb_reply($cbid, '', false);
						sm($chatID, $t, 'nascondi');
					} elseif ($msg) {
						sm($chatID, $t);
					} else {
						dm($chatID, $msgID);
					}
				}
				die;
			}
		} else {
			if ($typechat == "private") {
				if ($is_owner) {
					$t = getTranslate('disabledCloneAdmin');
				} else {
					$t = getTranslate('disabledCloneUser');
				}
				if ($cbid) {
					cb_reply($cbid, '', false);
					sm($chatID, $t, 'nascondi');
				} elseif ($msg) {
					sm($chatID, $t);
				} else {
					dm($chatID, $msgID);
				}
			}
			die;
		}
	} else {
		http_response_code(403);
		die;
	}
}

if (strpos($cmd, "getT ") === 0 and $isadmin) {
	$e = explode(" ", str_replace('/getT ', '', $msg));
	$lingua = $e[0];
	$place = $e[1];
	$ar = $e[2];
	sm($chatID, getTranslate($place, false, $lingua), false, '');
}

if ($cbdata == "viewAllLanguages") {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["langs-all", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$u['page'] = "langs-all";
	$cbdata = "changeLanguage";
} elseif ($cbdata == "viewAllLanguagesSecondPage") {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["langs-all-2", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$u['page'] = "langs-all-2";
	$cbdata = "changeLanguage";
} elseif ($cbdata == "viewSupportefLanguages") {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$u['page'] = " ";
	$cbdata = "changeLanguage";
} elseif ($cbdata == "viewSubLanguages") {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["sub-language", $userID], "no");
	if (!$q1['ok']) {
		cb_reply($cbid, "❌ " . getTranslate('generalError'), false);
		die;
	}
	$u['page'] = "sub-language";
	$cbdata = "changeLanguage";
}

if (isset($dialetti)) {
	foreach ($dialetti as $thislanguage => $sublangs) {
		if (in_array($lang, $sublangs)) {
			$princ_lang = $thislanguage;
		}
	}
	if (!isset($princ_lang)) $princ_lang = $lang;
} else {
	$princ_lang = $lang;
}

if ($cmd == "lang" or strpos($cbdata, "changeLanguage") === 0) {
	if (strpos($cbdata, "changeLanguage_") === 0) {
		$exlang = $lang;
		$lang = str_replace('changeLanguage_', '', $cbdata);
		if ($lang === $exlang or !$lang) {
			cb_reply($cbid, '', false);
			die;
		} else {
			cb_reply($cbid, '✅', false);
		}
		$q1 = db_query("UPDATE utenti SET lang = ? WHERE user_id = ?", [$lang, $userID], "no");
		if (!$q1['ok']) {
			cb_reply($cbid, "❌ " . getTranslate('generalError'), true);
			die;
		}
	} else {
		cb_reply($cbid, '', false);
	}
	unset($princ_lang);
	if (isset($dialetti)) {
		foreach ($dialetti as $thislanguage => $sublangs) {
			if (in_array($lang, $sublangs)) {
				$princ_lang = $thislanguage;
			}
		}
		if (!isset($princ_lang)) $princ_lang = $lang;
	} else {
		$princ_lang = $lang;
	}
	if ($u['page'] == "langs-all") {
		$cb = "viewSupportefLanguages";
		$emo = "➖";
		$langsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$langsname[$lang] = $langsname[$lang] . " ✅";
	} elseif ($u['page'] == "langs-all-2") {
		$cb = "viewSupportefLanguages";
		$emo = "➖";
		$page = true;
		$langsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$langsname[$lang] = $langsname[$lang] . " ✅";
	} elseif ($u['page'] == "sub-language") {
		$loaddef = true;
		$cb = "viewSupportefLanguages";
		$emo = "◀️";
		$flagsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
		$nlangsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$langsname = [];
		foreach ($dialetti[$princ_lang] as $thislang) {
			$langsname[$thislang] = $flagsname[$thislang] . " " . maiuscolo($nlangsname[$thislang]);
		}
		$langsname[$lang] = $langsname[$lang] . " ✅";
	} else {
		$cb = "viewAllLanguages";
		$emo = "➕";
		$langsname = json_decode('{"en":"🇬🇧 English","de":"🇩🇪 Deutsch","it":"🇮🇹 Italiano","pt":"🇧🇷 Português","he":"🇮🇱 עברית","uk":"🇺🇦 Українська","nb":"🇳🇴 Norsk","fa":"🇮🇷 پارسی","es":"🇪🇸 Español","zh_TW":"🇹🇼 台灣正體","zh_HK":"🇭🇰 港澳正體","ja":"🇨🇳 中囯簡体","ru":"🇷🇺 Русский","fr":"🇫🇷 Français","uz":"🇺🇿 O\'zbek"}', true);
		$langsname[$princ_lang] = $langsname[$princ_lang] . " ✅";
	}
	foreach ($langsname as $lingua => $nl) {
		if (!isset($num)) {
			$num = 0;
		} else {
			$num = $numSecur + 1;
		}
		$numSecur = $num;
		$nl = maiuscolo($nl);
		if ($u['page'] == "langs-all") {
			if ($numSecur == 45) {
				$menu[] = [
					[
						"text" => "▶️",
						"callback_data" => "viewAllLanguagesSecondPage"
					]
				];
				$finish = true;
			}
		} elseif ($u['page'] == "langs-all-2") {
			if ($numSecur == 46) {
				if (!isset($passato)) {
					$menu[] = [
						[
							"text" => "◀️",
							"callback_data" => "viewAllLanguages"
						]
					];
				}
				$finish = false;
				$passato = true;
				$num = $secnum;
			} elseif (!$passato) {
				$finish = true;
			} else {
				$finish = false;
			}
		}
		if (!$finish) {
			if ($secnum !== $num) {
				$secnum = $num + 1;
				if (isset($nl2)) {
					$menu[] = [
						[
							"text" => $nl2,
							"callback_data" => "changeLanguage_$lingua2"
						],
						[
							"text" => $nl,
							"callback_data" => "changeLanguage_$lingua"
						]
					];
				} else {
					//$finish = true;
					$menu[] = [
						[
							"text" => $nl,
							"callback_data" => "changeLanguage_$lingua"
						]
					];
				}
				unset($nl2);
			} else {
				$lingua2 = $lingua;
				$nl2 = $nl;
			}
		}
	}
	if (isset($nl2)) {
		$menu[] = [
			[
				"text" => $nl2,
				"callback_data" => "changeLanguage_$lingua2"
			]
		];
	}
	if (isset($dialetti[$princ_lang]) and !$loaddef) {
		$menu[] = [
			[
				"text" => "🗣 Sub languages",
				"callback_data" => "viewSubLanguages"
			]
		];
	}
	$menu[] = [
		[
			"text" => "💾 " . getTranslate('done'),
			"callback_data" => "settings"
		],
		[
			"text" => "$emo Other languages",
			"callback_data" => $cb
		]
	];
	if ($cmd) sm($chatID, getTranslate('setLanguage'), $menu);
	if ($cbdata) editMsg($chatID, getTranslate('setLanguage'), $cbmid, $menu);
	die;
}

if ($cmd == "update_languages" and $isadmin) {
	$config['json_payload'] = false;
	$m = sm($chatID, "Download lingue...");
	$r = oneskyapp2("/string/output", ['platform-id' => 164018]);
	foreach ($r['translation']['string_en.json'] as $ag => $agn) {
		$ag = explode("-", $ag)[0];
		$lingue_download[$ag] = $agn;
	}
	// Download delle stringhe
	$tg_languages_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
	$linguenew = $linguebot;
	unset($linguenew['en']);
	foreach($lingue_download as $lang => $agn) {
		foreach($agn as $ns => $str) {
			$linguenew[$lang][$ns] = $str;
		}
		$ok .= "\n✅ " . bold(maiuscolo($tg_languages_name[$lang]));
	}
	editMsg($chatID, $ok, $m['result']['message_id']);
	sd($chatID, "/home/masterpoll-documents/" . $f['languages'], "💾 Backup - " . date($u['settings']['date_format']));
	file_put_contents("/home/masterpoll-documents/" . $f['languages'], json_encode($linguenew, JSON_PRETTY_PRINT));
	die;
}

if ($cmd == "fix_languages" and $isadmin) {
	$config['json_payload'] = false;
	sd($chatID, "/home/masterpoll-documents/" . $f['languages'], "💾 Backup - " . date($u['settings']['date_format']));
	$strings = array_keys($linguebot['en']);
	foreach ($linguebot as $language => $stringhe) {
		foreach ($stringhe as $string => $text) {
			if (!in_array($string, $strings)) unset($linguebot[$language]);
		}
	}
	file_put_contents("/home/masterpoll-documents/" . $f['languages'], json_encode($linguebot, JSON_PRETTY_PRINT));
	sm($chatID, "✅ Fixed");
	die;
}

if ($cmd == "newlanguage" and $isadmin) {
	$toremove = [
		"addInstanceApproved",
		"addInstanceBePatientText",
		"addInstanceParameterList",
		"addInstanceRejected",
		"addInstanceSendToken",
		"addInstanceSetParameters",
		"addInstanceTechnicalIssue",
		"addInstanceToQueueButton",
		"clone",
		"messageIDInvalidWelcomeMessage",
		"pollOptionDeleteBoardHasChanged",
		"pollOptionDeleteBoardVoteNoKeep",
		"pollOptionDeleteBoardVoteYesDelete",
		"pollOptionDeleteErrorButPollStillFound",
		"tooManyRequestsByUser",
		"tooManyRequestsForMessage",
		"tooManyRequestsForPoll",
		"viewInBrowserExpansion",
		"setupPlease",
		"caughtScrewingWithCallbacks",
		"dontMessWithTheCommands",
		"donation",
		"pollTypeDescription",
		"settingsInformations",
		"shopList"
	];
	$tochange = [
		"startMessageAnonyButton" => "anonymousButton",
		"startMessagePersonalButton" => "personalButton",
		"startMessageChangeLanguageButton" => "changeLanguageButton",
		"startMessageChangePollTypeButton" => "changePollTypeButton",
		"startMessageChangeTimeZoneButton" => "changeTimeZoneButton"
	];
	$config['json_payload'] = false;
	sd($chatID, "/home/masterpoll-documents/" . $f['languages'], "💾 Backup - " . date($u['settings']['date_format']));
	$mid = sm($chatID, getTranslate('loading'))['result']['message_id'];
	$newlanguage = $linguebot;
	$totlang = count(array_keys($newlanguage));
	$totstrings = count($newlanguage['en']);
	foreach($newlanguage as $lang => $keys) {
		$range[] = true;
		foreach ($toremove as $stringa) {
			unset($newlanguage[$lang][$stringa]);
		}
		foreach($tochange as $oldstring => $newstring) {
			if ($newlanguage[$lang][$oldstring]) $newlanguage[$lang][$newstring] = $newlanguage[$lang][$oldstring];
			unset($newlanguage[$lang][$oldstring]);
		}
		/*file_put_contents("$lang.json", json_encode($newlanguage[$lang], JSON_PRETTY_PRINT));
		sd($chatID, "$lang.json", $langsname[$lang] . " - " . round(count($keys)/ $totstrings * 100) . "%");
		$t = "Languages loading - " . round(count($range) / $totlang * 100);
		editMsg($chatID, $t, $mid);
		sleep(1);
		unlink("$lang.json");*/
	}
	file_put_contents("/home/masterpoll-documents/" . $f['languages'], json_encode($newlanguage, JSON_PRETTY_PRINT));
	editMsg($chatID, "✅ Applicato il nuovo file delle lingue!", $mid);
	die;
}

if ($cmd == "translations" and $isadmin) {
	$langsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
	$languages_new = oneskyapp2("/platform/locales", ['platform-id' => 164018])['locales'];
	$r .= "\nFrom the platform\n";
	foreach ($languages_new as $language) {
		$thislang = explode('-', $language['locale'])[0];
		$planguage[$thislang] = $language['completeness'];
	}
	foreach($linguebot as $lang => $conts) {
		if (isset($planguage[$lang])) $r .= "<b>" . maiuscolo($langsname[$lang]) . ":</>  " . $planguage[$lang] . "% \n";
	}
	$r .= "\nFrom local file\n";
	$tot = count($linguebot['en']);
	foreach($linguebot as $lang => $conts) {
		$r .= "<b>" . maiuscolo ($langsname[$lang]) . ":</> " . explode('.', count($linguebot[$lang]) / $tot * 100)[0] . "% \n";
	}
	sm($chatID, bold("Translations of Master PollBot") . $r);
	die;
}

if (strpos($cmd, "import_lang") === 0 and $isadmin) {
	$q1 = db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["lang_import", $userID], "no");
	if (!$q1['ok']) {
		sm($chatID, "❌ " . getTranslate('generalError'));
		die;
	}
	sm($chatID, "Invia il file della singola lingua da importare.");
	die;
}

if ($u['page'] == "lang_import" and $file and $isadmin) {
	$e = explode(".", $file['file_name']);
	$ext = $e[count($e) - 1];
	if (strtolower($ext) !== "json") {
		sm($chatID, "Non è un file json.");
		die;
	}
	$config['json_payload'] = false;
	$cbmid = sm($chatID, "Scarico il file...")['result']['message_id'];
	db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID], "no");
	if (!$q1['ok']) {
		editMsg($chatID, "❌ " . getTranslate('generalError'), $cbmid);
		die;
	}
	copy(getFile($file['file_id']), "imported_language.json");
	$trylang = explode("-", str_replace(".json", '', $file['file_name']))[0];
	$cbdata = "importLanguage_$trylang";
	$cbid = false;
}

if (strpos($cbdata, "importLanguage_") === 0 and $isadmin) {
	$trylang = str_replace("importLanguage_", '', $cbdata);
	$langsname[$trylang] = $langsname[$trylang] . " ✅";
	foreach ($langsname as $lingua => $nl) {
		if (!isset($num)) {
			$num = 0;
		} else {
			$num = $numSecur + 1;
		}
		$numSecur = $num;
		$nl = maiuscolo($nl);
		if (!$finish) {
			if ($num == 0) {
				$secnum = 1;
			}
			if ($secnum !== $num) {
				$secnum = $num + 1;
				if (isset($nl2)) {
					$menu[] = [
						[
							"text" => $nl,
							"callback_data" => "importLanguage_$lingua"
						],
						[
							"text" => $nl2,
							"callback_data" => "importLanguage_$lingua2"
						],
					];
				} else {
					//$finish = true;
					$menu[] = [
						[
							"text" => $nl,
							"callback_data" => "importLanguage_$lingua"
						],
					];
				}
			} else {
				$lingua2 = $lingua;
				$nl2 = $nl;
			}
		}
	}
	$menu[] = [
		[
			"text" => "💾 " . getTranslate('done'),
			"callback_data" => "importLDone_$trylang"
		],
		[
			"text" => "❌ " . getTranslate('backButton'),
			"callback_data" => "importCancel"
		]
	];
	cb_reply($cbid, '', false, $cbmid, "Scegli la lingua: \n$stats", $menu);
	die;
}

if ($cbdata == "importCancel") {
	if (file_exists("imported_language.json")) unlink("imported_language.json");
	cb_reply($cbid, "Annullato", false, $cbmid, "Annullato...");
	die;
}

if (strpos($cbdata, "importLDone_") === 0 and $isadmin) {
	cb_reply($cbid, '👌', false);
	$trylang = str_replace("importLDone_", '', $cbdata);
	$backup = "backup-" . date("d-m-Y-H:i") . ".json";
	file_put_contents($backup, json_encode($linguebot));
	sd($chatID, $backup, "💾 Backup - " . date($u['settings']['date_format']));
	$linguebot[$trylang] = json_decode(file_get_contents("imported_language.json"), true);
	file_put_contents("/home/masterpoll-documents/" . $f['languages'], json_encode($linguebot, JSON_PRETTY_PRINT));
	editMsg($chatID, "✅ Lingua importata con successo!", $cbmid);
	if (file_exists("imported_language.json")) unlink("imported_language.json");
	if (file_exists($backup)) unlink($backup);
	die;
}
