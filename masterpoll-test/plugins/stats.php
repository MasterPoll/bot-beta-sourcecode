<?php

if ($cmd == "reactions" and $config['devmode']) {
	$menu[] = [
		[
			"text" => "🧐 34",
			"callback_data" => "okem"
		],
		[
			"text" => "😎 10",
			"callback_data" => "okem2"
		],
		[
			"text" => "😐 2",
			"callback_data" => "okem1"
		]
	];
	$menu[] = [
		[
			"text" => "Yes - 90%",
			"callback_data" => "oke"
		]
	];
	$menu[] = [
		[
			"text" => "No - 10%",
			"callback_data" => "okey"
		]
	];
	sm($chatID, "📊 <b>Vote poll for new feature</> \nYou can see it? \n\nYes [268] \n\nNo [26] \n\n👥 294 people have voted so far", $menu);
	die;
}

if ($isadmin) {
	if ($cmd == "advertisings") {
		$text = bold("💰 Sponsorizzazioni attive");
		foreach ($config['sponsor_messages'] as $region => $messages) {
			if ($region == "global") {
				$reg = "🌐 Global";
			} else {
				$reg = $langsname[$region];
			}
			$text .= "\n\n$reg";
			foreach ($messages as $id) {
				$text .= "\n- https://t.me/c/" . str_replace("-100", '', $config['sponsor']) ."/$id";
			}
		}
		sm($chatID, $text);
		die;
	} elseif ($cmd == "premiums") {
		$config['json_payload'] = false;
		$m = sm($chatID, "🕔 Caricamento...");
		$cbmid = $m['result']['message_id'];
		$allusers = db_query("SELECT settings, lang FROM utenti WHERE STRPOS(settings,?)>0", ['premium'], false);
		if ($allusers['ok']) {
			$allusers = $allusers['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
			die;
		}
		foreach ($allusers as $user) {
			if ($user['settings'] !== "[]") {
				$user['settings'] = json_decode($user['settings'], true);
				if (isset($user['settings']['premium'])) {
					$tpre += 1;
					$statsforlang[$user['lang']][] = $user;
					if ($user['settings']['premium'] == "lifetime") {
						$premium['lifetime'] += 1;
					} else {
						$premium['time'] += 1;
					}
				}
			}
		}
		$language_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$flag_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
		foreach ($statsforlang as $language => $users) {
			if (!$language_name[$language]) {
				$language_name[$language] = "Unknow";
			} else {
				$language_name[$language] = maiuscolo($language_name[$language]);
			}
			if ($language_name[$language] !== "Unknow" and $language_name[$language] !== $flag_name[$language]) $language_name[$language] = $flag_name[$language] . " " . $language_name[$language];
			$statslang .= "\n" . $language_name[$language] . ": " . count($users);
		}
		editMsg($chatID, bold("⭐️ VIP Stats") . "\n$statslang\n\n👑 Premium LifeTime: " . number_format($premium['lifetime']) . "\n🎩 Premium temporaneo: " . number_format($premium['time']) . "\n\n📋 Premium totali: " . number_format($tpre), $cbmid);
		die;
	} elseif ($cmd == "stats") {
		$config['response'] = true;
		$lgif = loading_gif();
		$m = sm($chatID, "<a href='$lgif'>&#8203</>Caricamento...", false, 'def', false, false);
		$msid = $m['result']['message_id'];
		$dislang = db_query("SELECT lang FROM (SELECT DISTINCT lang FROM utenti) utenti", false, false);
		if ($dislang['ok']) {
			$dislang = $dislang['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		$tt = [];
		$langsname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$flagname = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
		foreach($dislang as $user) {
			$q = db_query("SELECT COUNT(*) FROM utenti WHERE lang = ?", [$user['lang']], false);
			$languages[$user['lang']] = round($q['result']['count']);
			$tot += round($q['result']['count']);
			if (!in_array($user['lang'], array_keys($langsname))) {
				$nolanguages[$user['lang']] += 1;
			}
		}
		foreach($langsname as $language => $titolo) {
			if (!$languages[$language]) {
				unset($langsname[$language]);
			} else {
				if ($flagname[$language] !== $langsname[$language]) {
					$emo = $flagname[$language] . " ";
				} else {
					$emo = "";
				}
				$langsname[$language] = $emo . maiuscolo($titolo);
			}
		}
		if ($config['devmode']) {
			if (isset($nolanguages)) {
				foreach ($nolanguages as $langno => $sas) {
					$mlang .= "\n- $langno: $sas";
				}
				editMsg($chatID, "Lingue da rivedere: \n$mlang", $msid);
				die;
			}
		}
		foreach ($langsname as $lingua => $nl) {
			$num = $languages[$lingua];
			if ($num) {
				$all = $all + $num;
				$perc = " - " . round(($num / $tot) * 100, 2) . "%";
				$perce = round(($num / $tot) * 100, 1);
				if (round(($num / $tot) * 100) < 1) {
					$percar['Altro'] = $num + $percar['Altro'];
				} else {
					$percar[explode(" ", $nl)[1]] = $num;
				}
			} else {
				$perc = "";
				$perce = 0;
			}
			$mh[$num][] = $nl;
		}
		ksort($mh, SORT_NUMERIC);
		$mh = array_reverse($mh, true);
		foreach ($mh as $num => $nl) {
			foreach ($nl as $nl) {
				$perc = " - " . round(($num / $tot) * 100, 2) . "%";
				$r .= bold($nl . ":") . " $num$perc \n";
			}
		}
		$r .= bold("🏳️ Other: ") . round($tot - $all) . " - " . round((($tot - $all) / $tot) * 100, 2) . "% \n\n" . bold("🌐 Global:") . " $tot";
		$percar["Altro"] = ($tot - $all) + $percar["Altro"];
		editMsg($chatID, $r, $msid);
		$time_now = time();
		mb_internal_encoding("UTF-8");
		require($f['jpgraph.dir'] . 'jpgraph.php');
		require($f['jpgraph.dir'] . 'jpgraph_pie.php');
		$graph = new PieGraph(640, 640);
		//$config['log_report']['WARN'] = false;
		$theme_class = new VividTheme;
		$graph->SetTheme($theme_class);
		$graph->img->SetAntiAliasing(true);
		$graph->title->Set('Statistiche nazionali');
		$graph->SetBox(false);
		$p1 = new PiePlot(array_values($percar));
		$p1->SetLegends(array_keys($percar));
		$graph->Add($p1);
		$graph->Stroke("stats-$time_now.png");
		if (file_exists("stats-$time_now.png")) {
			$link = "<a href='https://" . $_SERVER['HTTP_HOST'] . "/" . str_replace($f['bot.dir'], '', realpath(".")) . "/stats-$time_now.png'>&#8203;</>";
			editMsg($chatID, $link . $r, $msid, false, 'def', false);
			sleep(3);
			unlink("stats-$time_now.png");
		}
		die;
	} elseif ($cmd == "graph") {
		$config['response'] = true;
		$m = sm($chatID, "Caricamento...");
		$msid = $m['result']['message_id'];
		$tt = db_query("SELECT first_update FROM utenti", false, false);
		if ($tt['ok']) {
			$tt = $tt['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $msid);
			die;
		}
		$stats = [];
		foreach ($tt as $user) {
			$stats[date("Y", $user['first_update'])][date("m", $user['first_update'])][date("d", $user['first_update'])] += 1;
		}
		$tot = count($tt);
		if ($stats === []) {
			editMsg($chatID, "Statistiche ancora vuote", $msid);
		} else {
			if (round(date("H")) < 12) {
				$time_now = time() - ((60 * 60 * 24) - 1);
			} else {
				$time_now = time();
			} 
			$time_first = round($time_now) - (60 * 60 * 24 * (31 - round(date('D'))));
			$r = range (30, 0);
			foreach ($r as $num) {
				$time_work = $time_now - (60 * 60 * 24 * $num);
				$data = date("d/m", $time_work);
				unset($count_now);
				foreach ($tt as $user) {
					if ($user['first_update'] <= $time_work) $count_now += 1;
				}
				$datas[$data] = $count_now;
				$guada[$data] = $stats[date("Y", $time_work)][date("m", $time_work)][date("d", $time_work)];
				if ($count_now == $guada[$data]) {
					$segno = "=";
				} else {
					$segno = "+";
				}
				if ($count_now <= 9) {
					$dse = "000" . $count_now;
				} elseif ($count_now <= 99) {
					$dse = "00" . $count_now;
				} elseif ($count_now <= 999) {
					$dse = "0" . $count_now;
				} else {
					$dse = $count_now;
				}
				if ($guada[$data] <= 9) {
					$dif = "00" . $$guada[$data];
				} elseif ($guada[$data] <= 99) {
					$dif = "0" . $$guada[$data];
				} else {
					$dif = $guada[$data];
				}
				$sta .= "\n$data → $count_now | $segno$dif";
				$last = $count_now;
			}
			require($f['jpgraph.dir'] . 'jpgraph.php');
			require($f['jpgraph.dir'] . 'jpgraph_line.php');
			$graph = new Graph(640, 360);
			$graph->SetScale("textlin");
			$theme_class = new UniversalTheme;
			$graph->SetTheme($theme_class);
			$graph->img->SetAntiAliasing(false);
			$graph->title->Set('Statistiche iscritti e sondaggi');
			$graph->SetBox(false);
			$graph->SetMargin(60,40,60,80);
			$graph->img->SetAntiAliasing();
			$graph->yaxis->HideZeroLabel();
			$graph->yaxis->HideLine(false);
			$graph->yaxis->HideTicks(false,false);
			$graph->xgrid->Show();
			$graph->xgrid->SetLineStyle("solid");
			$graph->xgrid->SetColor('#E3E3E3');
			$p1 = new LinePlot(array_values($datas));
			$graph->Add($p1);
			$p1->SetColor("#0000FF");
			$p1->SetLegend('Iscritti');
			$p2 = new LinePlot(array_values($guada));
			$graph->Add($p2);
			$p2->SetColor("#FF0000");
			$p2->SetLegend('Nuovi iscritti');
			$graph->legend->SetFrameWeight(1);
			$graph->Stroke("stats-$time_now.png");
			if (file_exists("stats-$time_now.png")) {
				$config['json_payload'] = false;
				$sp = sp($chatID, "https://" . $_SERVER['HTTP_HOST'] . "/" . str_replace($f['bot.dir'], '', realpath(".")) . "/stats-$time_now.png", bold("Statistiche del mese"));
				if (file_exists("stats-$time_now.png")) unlink("stats-$time_now.png");
				if ($sp['ok']) {
					dm($chatID, $msid);
					sm($chatID, code($sta));
				} else {
					editMsg($chatID, "Errore: " . $sp['description'], $msid);
				}
			}
		}
		die;
	} elseif ($cmd == "polls") {
		$config['json_payload'] = false;
		$m = sm($chatID, "🕔 Analizzo tutti i sondaggi...");
		$allpolls = db_query("SELECT settings FROM polls", false, false);
		if ($allpolls['ok']) {
			$allpolls = $allpolls['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
			die;
		}
		foreach ($allpolls as $poll) {
			$poll['settings'] = json_decode($poll['settings'], true);
			if (!$poll['settings']['type']) {
				$poll['settings']['type'] = "deleted";
			}
			$types[$poll['settings']['type']] = 1 + $types[$poll['settings']['type']];
		}
		foreach ($types as $type => $num) {
			$type = maiuscolo($type);
			$type[0] = strtolower($type[0]);
			if ($type !== "deleted") $r .= "\n" . bold(maiuscolo(getTranslate($type)) . ": ") . $num;
		}
		$callpolls = count($allpolls);
		$r = "<b>SONDAGGI DEL BOT</> <i>(" . ($callpolls - $types['deleted']) . "/" . $callpolls . ")</> $r\n<b>Eliminati: </b>" . $types['deleted'];
		editMsg($chatID, $r, $m['result']['message_id']);
		$time_now = time();
		require($f['jpgraph.dir'] . 'jpgraph.php');
		require($f['jpgraph.dir'] . 'jpgraph_pie.php');
		$graph = new PieGraph(640, 640);
		$theme_class = new VividTheme;
		$graph->SetTheme($theme_class);
		$graph->img->SetAntiAliasing(true);
		$graph->title->Set('Statistiche nazionali');
		$graph->SetBox(false);
		$p1 = new PiePlot(array_values($types));
		$p1->SetLegends(array_keys($types));
		$graph->Add($p1);
		$graph->Stroke("stats-$time_now.png");
		if (file_exists("stats-$time_now.png")) {
			$link = "<a href='https://" . $_SERVER['HTTP_HOST'] . "/" . str_replace($f['bot.dir'], '', realpath(".")) . "/stats-$time_now.png'>&#8203;</>";
			editMsg($chatID, $link . $r, $m['result']['message_id'], false, 'def', false);
			sleep(3);
			if (file_exists("stats-$time_now.png")) unlink("stats-$time_now.png");
		}
		die;
	} elseif ($cmd == "affiliato") {
		$config['response'] = true;
		$m = sm($chatID, "Caricamento...");
		$t = bold("Statistiche affiliato\n");
		$affiliato = json_decode(file_get_contents("/home/masterpoll-documents/affiliates.json"), true);
		$cu = count($affiliato);
		$mb = 0;
		$actv = [];
		$config['log_report']['WARN'] = false;
		foreach($affiliato as $user_id => $ainfo) {
			if ($ainfo['status']) $actv[$user_id] = true;
			$mb = $mb + $ainfo['money'];
			if (in_array($affiliato['users']) and $affiliato['users']) {
				foreach($affiliato['users'] as $bot_id => $users) {
					foreach($users as $user => $uinfos) {
						if (in_array("start", $uinfos)) $cstart[$bot_id][$user] = true;
					}
				}
			}
		}
		$cau = count($actv);
		$cstarts = count($cstart[$botID]);
		$t .= "\nAffiliati attivi: $cau/$cu";
		$t .= "\nSoldi generati: €" . number_format($mb / 100, 2);
		$t .= "\nStart ai Bot: $cstarts";
		editMsg($chatID, $t, $m['result']['message_id']);
		die;
	} elseif ($cmd == "apistats") {
		$config['json_payload'] = false;
		$m = sm($chatID, "🕔 Caricamento...");
		$cbmid = $m['result']['message_id'];
		$allusers = db_query("SELECT settings, lang FROM utenti", false, false);
		if ($allusers['ok']) {
			$allusers = $allusers['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
			die;
		}
		foreach ($allusers as $user) {
			if ($user['settings'] !== "[]") {
				$user['settings'] = json_decode($user['settings'], true);
				if (isset($user['settings']['token'])) {
					$tpre += 1;
					$statsforlang[$user['lang']][] = $user;
				}
			}
		}
		$language_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$flag_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
		foreach ($statsforlang as $language => $users) {
			if (!$language_name[$language]) {
				$language_name[$language] = "Unknow";
			} else {
				$language_name[$language] = maiuscolo($language_name[$language]);
			}
			if ($language_name[$language] !== "Unknow" and $language_name[$language] !== $flag_name[$language]) $language_name[$language] = $flag_name[$language] . " " . $language_name[$language];
			$statslang .= "\n" . $language_name[$language] . ": " . count($users);
		}
		editMsg($chatID, bold("👨🏻‍💻 API di MasterPoll") . "\n$statslang\n\n📋 Dev totali: " . number_format($tpre), $cbmid);
		die;
	} elseif ($cmd == "walletstats") {
		$config['json_payload'] = false;
		$m = sm($chatID, "🕔 Caricamento...");
		$cbmid = $m['result']['message_id'];
		$allusers = db_query("SELECT user_id, nome, settings, lang FROM utenti WHERE strpos(settings,?)>0", ['wallet'], false);
		if ($allusers['ok']) {
			$allusers = $allusers['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
			die;
		}
		foreach ($allusers as $user) {
			if ($user['settings'] !== "[]") {
				$user['settings'] = json_decode($user['settings'], true);
				if (isset($user['settings']['wallet']) and $user['settings']['wallet']) {
					$tpre += 1;
					$statsforlang[$user['lang']][] = $user;
					$solditot += $user['settings']['wallet'];
				}
			}
		}
		$language_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_name.json"), true);
		$flag_name = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
		if ($statsforlang) {
			foreach ($statsforlang as $language => $users) {
				if (!$language_name[$language]) {
					$language_name[$language] = "Unknow";
				} else {
					$language_name[$language] = maiuscolo($language_name[$language]);
				}
				if ($language_name[$language] !== "Unknow" and $language_name[$language] !== $flag_name[$language]) $language_name[$language] = $flag_name[$language] . " " . $language_name[$language];
				$statslang .= "\n\n" . $language_name[$language] . ": " . count($users);
				foreach ($users as $user) {
					$statslang .= "\n- " . tag($user['user_id'], $user['nome']) . ": €" . number_format($user['settings']['wallet'] / 100, 2);
				}
			}
		}
		editMsg($chatID, bold("👨🏻‍💻 Portafogli di MasterPoll") . "$statslang\n\n💰 Soldi totali: €" . number_format($solditot / 100, 2) . "\n📋 Utenti totali: " . number_format($tpre), $cbmid);
		die;
	} elseif (strpos($cmd, "top") === 0) {
		$config['json_payload'] = false;
		$m = sm($chatID, "Carico...");
		$cbmid = $m['result']['message_id'];
		$allpolls = db_query("SELECT * FROM polls WHERE status = ?", ["open"], false);
		if ($allpolls['ok']) {
			$allpolls = $allpolls['result'];
		} else {
			editMsg($chatID, "❌ " . getTranslate('generalError'), $m['result']['message_id']);
			die;
		}
		$t = bold("🔝 Top sondaggi più votati");
		if (strpos($cmd, "top_") === 0) {
			$type = str_replace("top_", '', $cmd);
			if (!in_array($type, $config['types'])) {
				editMsg($chatID, "Tipo di sondaggio non valido...");
				die;
			}
		}
		foreach ($allpolls as $poll) {
			if (isset($poll['settings'])) {
				$poll['settings'] = json_decode($poll['settings'], true);
				if (isset($poll['settings']['language'])) {
					$poll['lang'] = $poll['settings']['language'];
				} else {
					$poll['lang'] = 'default';
				}
				$poll['type'] = $poll['settings']['type'];
				if (isset($type)) {
					if ($poll['type'] == $type) {
						$okty = true;
					} else {
						$okty = false;
					}
				} else {
					$okty = true;
				}
				if ($okty) {
					$poll['choices'] = json_decode($poll['choices'], true);
					if ($poll['type'] !== "board") {
						$users = [];
						$chosenChoice = [];
						if ($poll['choices']) {
							foreach ($poll['choices'] as $optionableChoice => $keys) {
								$or = count($keys);
								$chosenChoice[$optionableChoice] = $or;
								$votes = $or + $votes;
								foreach ($keys as $id) {
									$users[$id] = true;
								}
							}
						}
					} else {
						$users = [];
						$chosenChoice = $poll['choices'];
						if ($poll['choices']) {
							foreach ($poll['choices'] as $user => $text) {
								$users[$user] = true;
							}
							$poll['choices'] = count($users);
						} else {
							$users = [];
							$poll['choices'] = 0;
						}
					}
					if (!count($users)) {
						$usercount = "0";
					} else {
						$usercount = count($users);
					}
					$ivotes[$usercount][] = $poll;
				}
			}
		}
		if (!$ivotes) {
			editMsg($chatID, "Nessun risultato...", $cbmid);
			die;
		} else {
			ksort($ivotes, SORT_NUMERIC);
			$ivotes = array_reverse($ivotes, true);
			$flags = json_decode(file_get_contents("/home/masterpoll-documents/tg_languages_flag.json"), true);
			foreach ($ivotes as $voti => $polls) {
				$num += 1;
				if ($num <= 10) {
					foreach ($polls as $poll) {
						$t .= "\n\n$num) " . italic(mb_substr($poll['title'], 0, 32));
						if ($poll['anonymous']) {
							$anon = "✅";
						} else {
							$anon = "☑️";
						}
						if ($poll['lang'] === 'default') $poll['lang'] = getLanguage($poll['user_id']);
						$language_name = $flags[$poll['lang']];
						$t .= "\n├ " . code("/spoll_" . $poll['poll_id'] . "-" . $poll['user_id']) . "\n├ " . bold("Lingua:") . " $language_name\n├ " . bold("Voti:") . " $voti\n├ " . bold(getTranslate('pollType') . ":") . " " . $poll['type'] . " \n└ " . bold(getTranslate('pollAnonymous') . ":") . $anon;
					}
				}
			}
		}
		editMsg($chatID, $t, $cbmid);
		die;
	}

}
